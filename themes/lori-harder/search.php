<?php
/**
 * Search Page
 * 
 */

get_header();
?>
<article>

<?php //Hero Header
	if(get_field('header','option') == "true") { 
	
	//var
	$hero_ht = get_field('hero_header_height', 'option');
	$bg_hero_img = get_field('header_image', 'option');
	$bg_hero_tint = get_field('hero_header_tint', 'option');
	$bg_hero_text = get_field('header_text', 'option');	
?>

<!-- START HERO HEADER -->
<section class="hero-header blog-header" style="background-image:linear-gradient(rgba(0,0,0, <?php echo $bg_hero_tint ?>), rgba(0,0,0, <?php echo $bg_hero_tint ?>)),url(<?php echo $bg_hero_img['url']; ?>);min-height:<?php echo $hero_ht; ?>">
	<div class="container">
		<div class="hero-text-outer" style="height:<?php echo $hero_ht; ?>;<?php echo the_field('header_text_width_position_hero', 'option') ?>">
			<div class="hero-text"><?php echo $bg_hero_text; ?></div>
		</div>
	</div>
</section>
<!-- END HERO HEADER -->
<?php } ?>

	<div class="container">
		<?php if (have_posts()) : ?>

	        <h3 class="center">Results for <?php echo $s; ?></h3>

				<?php get_template_part( 'loop', 'archive'); ?>

				<?php get_template_part( '_template-parts/part', 'navigation' ); ?>

		<?php else : ?>

				<h1 class="heading">Not Found</h1>
				<p class="center">Sorry, but you are looking for something that isn't here.</p>
				<?php get_search_form(); ?>

		<?php endif; ?>
	</div>
</article>

<?php get_footer(); ?>
