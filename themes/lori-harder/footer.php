        <div style="clear:both;"></div>

    </div><!--CONTAINER-->

</div><!--OUTER WRAPPER-->
<?php 
	if (is_front_page() || is_page_template('_template-parts/template_about_page.php')):
		echo do_shortcode('[instashow id="1"]'); 
	endif;
?>
<section class="top-footer">
	<div class="container">
	<div id="footer-mobile-menu"><?php if(is_active_sidebar('footer_mobile_menu')) dynamic_sidebar('footer_mobile_menu'); ?></div>
		<div class="row">
			<div class="six columns top-footer-left">
				<div class="one-quarter mobile-hide"><?php if(is_active_sidebar('top_footer_menu_1')) dynamic_sidebar('top_footer_menu_1'); ?></div>
				<div class="one-quarter mobile-hide"><?php if(is_active_sidebar('top_footer_menu_2')) dynamic_sidebar('top_footer_menu_2'); ?></div>
				<div class="one-quarter mobile-hide"><?php if(is_active_sidebar('top_footer_menu_3')) dynamic_sidebar('top_footer_menu_3'); ?></div>
				<div class="one-quarter last"><?php if(is_active_sidebar('top_footer_social')) dynamic_sidebar('top_footer_social'); ?></div>
			</div>
			<div class="three columns top-footer-optin-text">
			<?php if(is_active_sidebar('top_footer_optin_text')) dynamic_sidebar('top_footer_optin_text'); ?>
			</div>
			<div class="three columns top-footer-form">
			<?php the_field( 'form_code', 'option' ); ?>
			<script>jQuery(document).ready(function($){ $(".top-footer #mr-field-element-195998811846").prop('value', 'Get Fit'); });</script>
			</div>
		</div>
	</div>
</section>
<footer>
	<div id="credits" class="container">
		<div class="footer-left u-pull-left">COPYRIGHT &copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?><?php if(is_active_sidebar('footer_menu')) dynamic_sidebar('footer_menu'); ?></div>
		<div class="footer-right u-pull-right">Designed by <a href="http://rachelpesso.com/" target="_blank">Rachel Pesso</a> + Development by <a href="http://www.alchemyandaim.com/" target="_blank">Alchemy + Aim</a></div>
	</div>
</footer>

</div>

<div id="search-overlay">
  <h6>Start typing and press enter to search</h6>
  <?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
  <div class="close-button"></div>
</div>

<?php wp_footer(); ?>

<?php //Google Analytics
	if(get_field('google_analytics_location', 'option') == "footer") {
    	echo get_field('google_analytics_code', 'option');
	}
?>

<?php //Custom Javascript
	$js = get_field('custom_javascript' ,'option');
	if( !empty($js) ): ?>
    	<script>
		(function($) {
        <?php echo $js; ?>
		})(jQuery);
    	</script>
<?php endif; ?>
</body>
</html>