<?php
/*
Template Name: Bliss Project Sales Page
*/
?>
<?php get_header(); ?>
<article>   
	<style>.hero-header{margin: 0 auto -20px; }</style>
<?php //Hero Header
	if(get_field('header') == "true") { 
	
	//var
	$hero_ht = get_field('hero_header_height');
	$bg_hero_img = get_field('header_image');
	$bg_hero_tint = get_field('header_tint');
	$bg_hero_text = get_field('header_text');	
?>

<!-- START HERO HEADER -->
<section class="hero-header" style="background-image:linear-gradient(rgba(226,212,203, <?php echo $bg_hero_tint ?>), rgba(226,212,203, <?php echo $bg_hero_tint ?>)),url(<?php echo $bg_hero_img['url']; ?>);min-height:<?php echo $hero_ht; ?>">
	<div class="container">
		<div class="hero-text-outer" style="height:<?php echo $hero_ht; ?>;<?php echo the_field('header_text_width_position_hero') ?>">
			<div class="hero-text"><?php echo $bg_hero_text; ?></div>
		</div>
	</div>
</section>
<!-- END HERO HEADER -->
<?php } ?>

<!-- INTRO SECTION -->
<section class="bliss-intro">
	<div class="container">
		<div class="row">
			<div class="five columns">
				<?php the_field( 'intro_text' ); ?>
			</div>
			<div class="seven columns">
				<?php 	//var
				$vid_id = get_field( 'youtube_video_id' );
				$attachment_id = get_field('video_image');
				$size = "full"; // (thumbnail, medium, large, full or custom size)
				$vid_image = wp_get_attachment_image_src( $attachment_id, $size );
				// url = $image[0];
				// width = $image[1];
				// height = $image[2];
				?>
				<div class="video-container">
    				<div class="video">
    					<img src="<?php echo $vid_image[0]; ?>">
    					<!-- <iframe width="987" height="508" src="http://www.youtube.com/embed/<?php echo $vid_id; ?>?autoplay=1" frameborder="0" allowfullscreen></iframe>-->
    				</div>
    			</div>
			</div>
		</div>
	</div>
</section>

<!-- BLISS CONTENT SECTIONS -->
<?php if ( have_rows( 'bliss_flex_content' ) ): ?>
	<?php while ( have_rows( 'bliss_flex_content' ) ) : the_row(); ?>
		<?php if ( get_row_layout() == 'wysiwyg_section' ) : ?>
		<section id="<?php the_sub_field( 'section_id' ); ?>">
		<div class="container">
		<div class="row">
			<div class="inner-text"><?php the_sub_field( 'general_text_section' ); ?></div>
			</div>
			</div>
	</section>
		<?php elseif ( get_row_layout() == 'two_image_section' ) : ?>
			<?php if ( have_rows( 'two_background_images' ) ) : ?>
			<section class="two-images">
			<div class="row">
				<?php while ( have_rows( 'two_background_images' ) ) : the_row(); ?>
					<?php $two_background_image = get_sub_field( 'two_background_image' ); ?>
					<?php if ( $two_background_image ) { ?>
					
				<div class="image-bg-two columns" style="background-image:url(<?php echo $two_background_image['url']; ?>);"></div>
					<?php } ?>
				<?php endwhile; ?>
				</div>
					</section>
			<?php else : ?>
				<?php // no rows found ?>

			<?php endif; ?>
		<?php elseif ( get_row_layout() == 'three_image_section' ) : ?>
			<?php if ( have_rows( 'three_background_images' ) ) : ?>
			<section class="three-images">
			<div class="row">
				<?php while ( have_rows( 'three_background_images' ) ) : the_row(); ?>
					<?php $three_background_image = get_sub_field( 'three_background_image' ); ?>
					<?php if ( $three_background_image ) { ?>
					<div class="image-bg-three columns" style="background-image:url(<?php echo $three_background_image['url']; ?>);"></div>
					<?php } ?>
				<?php endwhile; ?>
				</div>
		</section>
			<?php else : ?>
				<?php // no rows found ?>
			<?php endif; ?>
		<?php elseif ( get_row_layout() == 'testimonial_section' ) : ?>
		<section class="bliss-textimonial">
		<div class="container">
		<div class="row">
			<?php $testimonial_author_image = get_sub_field( 'testimonial_author_image' ); ?>
			<?php if ( $testimonial_author_image ) { ?>
			<div class="bliss-textimonial-author"><div class="border-one"></div><div class="border-two"></div><div class="auth-img"><?php echo wp_get_attachment_image( $testimonial_author_image, 'bliss-auth' ); ?></div></div>
			<?php } ?>
			<div class="bliss-testimonial-text"><?php the_sub_field( 'testimonial_author_text' ); ?></div>
		<?php endif; ?>
			</div>
			</div>
	</section>
	<?php endwhile; ?>
<?php else: ?>
	<?php // no layouts found ?>
<?php endif; ?>


<!-- EVENT DATES SECTIONS -->

<?php $background_image = get_field( 'background_image' ); ?>
<?php if ( $background_image ) { ?>
<section class="event-dates" style="background-image:linear-gradient(rgba(255,255,255,.16), rgba(255,255,255,.16)),url(<?php echo $background_image['url']; ?>);">
<?php } ?>
	<div class="date-panel"><div class="date-panel-inner"><?php the_field( 'event_dates_text' ); ?></div></div>	
	</section>

<!-- FOR YOU SECTION -->
<section class="for-you">
<div class="container">
<div class="row">
	<h1><?php the_field( 'for_you_heading' ); ?></h1>
	<div class="for-you-list"><?php the_field( 'for_you_text' ); ?></div>
	</div>
	</div>
	</section>

<!-- FAQ SECTION -->
<section class="bliss-faq">
<div class="container">
	<h4><?php the_field( 'faq_heading_text' ); ?></h4>

<?php if ( have_rows( 'faqs' ) ) : ?>
	<?php while ( have_rows( 'faqs' ) ) : the_row(); ?>
	<div class="toggle">
	<div class="toggle-title"><?php the_sub_field( 'faq_question' ); ?></div>
	<div class="togglebox"><div class="toggle-content"><?php the_sub_field( 'faq_answer' ); ?></div></div>
	</div>
	<?php endwhile; ?>
<?php else : ?>
	<?php // no rows found ?>
<?php endif; ?>
	</div>
	</section>

<!-- SPONSOR SECTION -->
<section class="bliss-sponsors">
<div class="container">
<?php the_field( 'sponsor_text' ); ?>
	</div>
	</section>

</article>
<?php get_footer(); ?>