<?php
/*
Template Name: Cookbook Page
*/
?>
<?php get_header(); ?>

<article>

<?php //Hero Header
	if(get_field('header') == "true") { 
	
	//var
	$hero_ht = get_field('hero_header_height');
	$bg_hero_img = get_field('header_image');
	$bg_hero_tint = get_field('header_tint');
	$bg_hero_text = get_field('header_text');	
?>

<!-- START HERO HEADER -->
<section class="hero-header" style="background-image:linear-gradient(rgba(0,0,0, <?php echo $bg_hero_tint ?>), rgba(0,0,0, <?php echo $bg_hero_tint ?>)),url(<?php echo $bg_hero_img['url']; ?>);min-height:<?php echo $hero_ht; ?>">
	<div class="container">
		<div class="hero-text-outer" style="height:<?php echo $hero_ht; ?>;<?php echo the_field('header_text_width_position_hero') ?>">
			<div class="hero-text"><?php echo $bg_hero_text; ?></div>
		</div>
	</div>
</section>
<!-- END HERO HEADER -->
<?php } ?>


<section class="cookbook-content">
<div class="container">
<div class="row">
<div class="seven columns">
<?php the_field( 'cookbook_text' ); ?>
	</div>
	<div class="four columns offset-by-one ipad-img">
<?php $cookbook_image = get_field( 'cookbook_image' ); ?>
<?php if ( $cookbook_image ) { ?>
	<img src="<?php echo $cookbook_image['url']; ?>" alt="<?php echo $cookbook_image['alt']; ?>" />
<?php } ?>
	</div>
	</div>
	</div>
	</section>	
	
	
	<?php if ( have_rows( 'testimonials' ) ) : ?>
	<section class="cookbook-testimonials">
	<div class="container">
<div class="row">
	<?php while ( have_rows( 'testimonials' ) ) : the_row(); ?>
	<div class="four columns">
	<div class="cookbook-testimonial">
		<div class="auth-image"><?php $author_image = get_sub_field( 'author_image' ); ?>
		<?php if ( $author_image ) { ?>
			<img src="<?php echo $author_image['url']; ?>" alt="<?php echo $author_image['alt']; ?>" />
		<?php } ?>
		</div>
		<div class="auth-text"><?php the_sub_field( 'testimonial_text' ); ?></div>
	</div>
	</div>
	<?php endwhile; ?>
<?php else : ?>
	<?php // no rows found ?>
		
<?php endif; ?>
</div>
		</div>
		</section>

	
		<section class="cookbook-callout">
		<div class="container">
<div class="row">
	<div class="twelve columns">
		<?php the_field( 'callout_text' ); ?>
	</div>
			</div>
			</div>
	</section>
	
	

</article>
<?php get_footer(); ?>