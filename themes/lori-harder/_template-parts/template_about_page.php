<?php
/*
Template Name: About Page
*/
?>
<?php get_header(); ?>
<article>   
	<style>.hero-header{margin: 0 auto -100px; }</style>
<?php //Hero Header
	if(get_field('header') == "true") { 
	
	//var
	$hero_ht = get_field('hero_header_height');
	$bg_hero_img = get_field('header_image');
	$bg_hero_tint = get_field('header_tint');
	$bg_hero_text = get_field('header_text');	
?>

<!-- START HERO HEADER -->
<section class="hero-header" style="background-image:linear-gradient(rgba(0,0,0, <?php echo $bg_hero_tint ?>), rgba(0,0,0, <?php echo $bg_hero_tint ?>)),url(<?php echo $bg_hero_img['url']; ?>);min-height:<?php echo $hero_ht; ?>">
	<div class="container">
		<div class="hero-text-outer" style="height:<?php echo $hero_ht; ?>;<?php echo the_field('header_text_width_position_hero') ?>">
			<div class="hero-text"><?php echo $bg_hero_text; ?></div>
		</div>
	</div>
</section>
<!-- END HERO HEADER -->
<?php } ?>

<section class="dream-big">
	<div class="container">
		<div class="row">
			<div class="five columns">
				<?php the_field( 'dream_big_text' ); ?>
			</div>
			<div class="offset-by-one six columns dream-big-img-one">
<?php $dream_big_image_1 = get_field( 'dream_big_image_1' ); ?>
<?php if ( $dream_big_image_1 ) { ?>
	<img src="<?php echo $dream_big_image_1['url']; ?>" alt="<?php echo $dream_big_image_1['alt']; ?>" />
<?php } ?>
			</div>
		</div>
		<div class="row">
			<div class="eight columns dream-big-img-two">
<?php $dream_big_image_2 = get_field( 'dream_big_image_2' ); ?>
<?php if ( $dream_big_image_2 ) { ?>
	<img src="<?php echo $dream_big_image_2['url']; ?>" alt="<?php echo $dream_big_image_2['alt']; ?>" />
<?php } ?>
			</div>
		</div>
	</div>
</section>

<section class="my-story">
	<div class="container">
		<div class="row">
			<div class="twelve columns">
				<?php the_field( 'my_story_text' ); ?>
			</div>
		</div>
	</div>
</section>

<section class="zines">
	<div class="container">
<?php if ( have_rows( 'magazines' ) ) : ?>
<ul>
	<?php while ( have_rows( 'magazines' ) ) : the_row(); ?>
	<?php $mag = get_sub_field( 'magazine' ); ?>
		<?php if ( $mag ) { ?>
	<li><img src="<?php echo $mag['url']; ?>" alt="<?php echo $mag['alt']; ?>" /></li>
		<?php } ?>
	<?php endwhile; ?>
<?php else : ?>
	<?php // no rows found ?>
<?php endif; ?>
		</ul>
	</div>
</section>

<section class="logo-section">
	<div class="container">
<?php if ( have_rows( 'logos' ) ) : ?>
		<ul>
	<?php while ( have_rows( 'logos' ) ) : the_row(); ?>
		<?php $logo = get_sub_field( 'logo' ); ?>
		<?php if ( $logo ) { ?>
			<li><img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" /></li>
		<?php } ?>
		<?php the_sub_field( 'logo_link' ); ?>
	<?php endwhile; ?>
<?php else : ?>
	<?php // no rows found ?>
<?php endif; ?>
		</ul>
	</div>
</section>
	
</article>
<?php get_footer(); ?>