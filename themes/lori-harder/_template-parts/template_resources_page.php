<?php
/*
Template Name: Resources Page
*/
?>
<?php get_header(); ?>

<article>

<?php //Hero Header
	if(get_field('header') == "true") { 
	
	//var
	$hero_ht = get_field('hero_header_height');
	$bg_hero_img = get_field('header_image');
	$bg_hero_tint = get_field('header_tint');
	$bg_hero_text = get_field('header_text');	
?>

<!-- START HERO HEADER -->
<section class="hero-header" style="background-image:linear-gradient(rgba(0,0,0, <?php echo $bg_hero_tint ?>), rgba(0,0,0, <?php echo $bg_hero_tint ?>)),url(<?php echo $bg_hero_img['url']; ?>);min-height:<?php echo $hero_ht; ?>">
	<div class="container">
		<div class="hero-text-outer" style="height:<?php echo $hero_ht; ?>;<?php echo the_field('header_text_width_position_hero') ?>">
			<div class="hero-text"><?php echo $bg_hero_text; ?></div>
		</div>
	</div>
</section>
<!-- END HERO HEADER -->
<?php } ?>


<section class="product-content">
<div class="container">
<h1><?php the_title(); ?></h1>
<!-- PRODUCT LIST AREA -->
<div class="product-list">
<?php
	
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	$query = new WP_Query( array('post_type' => 'resources', 'posts_per_page' => -1, 'paged' => $paged ) );
 	if ( have_posts() ) while ( $query->have_posts() ) : $query->the_post(); 
 	$background = wp_get_attachment_image_src( get_post_thumbnail_id( $page->ID ), 'full' ); ?>
 	 <div class="product">
 	<div class="image-text-section">
	<div class="image" style="background-image:url(<?php echo $background[0]; ?>);"></div>
    <div class="item-content">
    <div class="item-content-inner">
 			<h1><?php the_title(); ?></h1>
			 <?php the_field( 'product_or_resource_excerpt' ); ?>
			 <?php if ( get_field( 'off-site_url' ) == 1 ) { ?>
	 <a href="<?php the_field( 'button_link' ); ?>" class="button button-primary" target="_blank"><?php the_field( 'button_label' ); ?></a>
<?php } else { ?>
	 <a href="<?php the_permalink(); ?>" class="button button-primary"><?php the_field( 'button_label' ); ?></a>
<?php } ?>
	</div>
		 </div>
</div>
	</div>
     
<?php endwhile; ?>

<div id="page_nav">
	<div class="u-pull-right"><?php echo next_posts_link( 'Next Page', $query->max_num_pages ); ?></div>
	<div class="u-pull-left"><?php echo previous_posts_link( 'Previous Page' ); ?></div>
	</div>

	</div>
<!-- END PRODUCT LIST AREA -->
	</div>
</section>
</article>
<?php get_footer(); ?>