<?php
/*
Template Name: 14 Day Challenge Sales Page
*/
?>
<?php get_header(); ?>
<article>   
	<style>header,.top-footer{display:none;}.hero-header{margin: 0 auto -50px;top:-100px;}</style>
<?php //Hero Header
	if(get_field('header') == "true") { 
	
	//var
	$hero_ht = get_field('hero_header_height');
	$bg_hero_img = get_field('header_image');
	$bg_hero_tint = get_field('header_tint');
	$bg_hero_text = get_field('header_text');	
?>

<!-- START HERO HEADER -->
<section class="hero-header" style="background-image:linear-gradient(rgba(0,0,0, <?php echo $bg_hero_tint ?>), rgba(0,0,0, <?php echo $bg_hero_tint ?>)),url(<?php echo $bg_hero_img['url']; ?>);min-height:<?php echo $hero_ht; ?>">
	<div class="container">
		<div class="hero-text-outer" style="height:<?php echo $hero_ht; ?>;<?php echo the_field('header_text_width_position_hero') ?>">
			<div class="hero-text"><?php echo $bg_hero_text; ?></div>
		</div>
	</div>
</section>
<!-- END HERO HEADER -->
<?php } ?>

<!-- INTRO SECTION -->
<section class="lh14-day-intro">
	<div class="container">
		<div class="row">
			<div class="seven columns">
				<div class="lh14-day-intro-text">
				<?php the_field( 'intro_text' ); ?>
				</div>
			</div>
			<div class="five columns">
				<div class="lh14-day-optin">
			<?php the_field( 'opt_in_form_text' ); ?>
			<?php the_field( 'opt_in_form_code' ); ?>
			<?php $btn_label = get_field( 'opt_in_submit_button_label' ); ?>
			<script>jQuery(document).ready(function($){ $(".lh14-day-optin #mr-field-element-195998811846").prop('value', '<?php echo $btn_label; ?>'); });</script>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- HEY GORGEOUS SECTION -->
<section class="hey-gorgeous">
	<div class="container">
		<div class="row">
			<div class="hey-gorgeous-text"><?php the_field( 'hey_gorgeous_text' ); ?></div>
		</div>
	</div>
</section>

<!-- HOW IT WORKS SECTION -->
<section class="how-it-works">
	<div class="container">
		<h1 class="center"><?php the_field( 'how_it_works_header_text' ); ?></h1>
		<div class="row">
			<div class="four columns">
			<?php the_field( 'callouts_left' ); ?>
			</div>
			<?php $how_it_works_image = get_field( 'how_it_works_image' ); ?>
			<?php if ( $how_it_works_image ) { ?>
			<div class="four columns">
				<img src="<?php echo $how_it_works_image['url']; ?>" alt="<?php echo $how_it_works_image['alt']; ?>" />
			</div>
			<?php } ?>
			<div class="four columns">
				<?php the_field( 'callouts_right' ); ?>
			</div>
		</div>
	</div>
</section>

<!-- JOIN FOOTER SECTION -->
<section class="join-footer">
	<div class="container">
		<div class="row">
			<div class="join-container">
			<?php the_field( 'join_footer_text' ); ?>
			<div class="center"><a href="<?php the_field( 'button_link' ); ?>" class="button button-primary"><?php the_field( 'button_label' ); ?></a></div>
			</div>
		</div>
	</div>
</section>

	
</article>
<?php get_footer(); ?>