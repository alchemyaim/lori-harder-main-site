<?php
/*
Template Name: Body + Soul Sales Page
*/
?>
<?php get_header(); ?>
<article>   
<?php //Hero Header
	if(get_field('header') == "true") { 
	
	//var
	$hero_ht = get_field('hero_header_height');
	$bg_hero_img = get_field('header_image');
	$bg_hero_tint = get_field('header_tint');
	$bg_hero_text = get_field('header_text');	
?>

<!-- START HERO HEADER -->
<section class="hero-header" style="background-image:linear-gradient(rgba(212,115,192, <?php echo $bg_hero_tint ?>), rgba(212,115,192, <?php echo $bg_hero_tint ?>)),url(<?php echo $bg_hero_img['url']; ?>);min-height:<?php echo $hero_ht; ?>">
	<div class="container">
		<div class="hero-text-outer" style="height:<?php echo $hero_ht; ?>;<?php echo the_field('header_text_width_position_hero') ?>">
			<div class="hero-text"><?php echo $bg_hero_text; ?></div>
		</div>
	</div>
</section>
<!-- END HERO HEADER -->
<?php } ?>

<!-- INTRO SECTION -->
<section class="body-soul-intro">
	<div class="container">
		<div class="row">
			<div class="five columns">
				<?php the_field( 'intro_text' ); ?>
			</div>
			<div class="seven columns">
				<?php 	//var
				$vid_id = get_field( 'youtube_video_id' );
				$attachment_id = get_field('video_image');
				$size = "full"; // (thumbnail, medium, large, full or custom size)
				$vid_image = wp_get_attachment_image_src( $attachment_id, $size );
				// url = $image[0];
				// width = $image[1];
				// height = $image[2];
				?>
				<div class="video-container">
    				<div class="video">
    					<img src="<?php echo $vid_image[0]; ?>">
    					<!-- <iframe width="987" height="508" src="http://www.youtube.com/embed/<?php echo $vid_id; ?>?autoplay=1" frameborder="0" allowfullscreen></iframe>-->
    				</div>
    			</div>
			</div>
		</div>
	</div>
	
	<?php if ( have_rows( 'photo_row' ) ) : ?>
	<div class="photo-row">
	
	<?php while ( have_rows( 'photo_row' ) ) : the_row(); ?>
		<?php $photo_row_photos = get_sub_field( 'photo_row_photos' ); ?>
		<?php if ( $photo_row_photos ) { ?>
		<div><img src="<?php echo $photo_row_photos['url']; ?>" alt="<?php echo $photo_row_photos['alt']; ?>" /></div>
		<?php } ?>
	<?php endwhile; ?>
<?php else : ?>
	<?php // no rows found ?>
	
<?php endif; ?>
</section>

<!-- BODY + SOUL CONTENT SECTIONS -->
<?php if ( have_rows( 'body_soul_flex_content' ) ): ?>
	<?php while ( have_rows( 'body_soul_flex_content' ) ) : the_row(); ?>
		<?php if ( get_row_layout() == 'wysiwyg_section' ) : ?>
		<section id="<?php the_sub_field( 'section_id' ); ?>">
		<div class="container">
		<div class="row">
			<div class="inner-text"><?php the_sub_field( 'general_text_section' ); ?></div>
			</div>
			</div>
	</section>
		<?php elseif ( get_row_layout() == 'testimonial_section' ) : ?>
		<section class="bliss-textimonial">
		<div class="container">
		<div class="row">
			<?php $testimonial_author_image = get_sub_field( 'testimonial_author_image' ); ?>
			<?php if ( $testimonial_author_image ) { ?>
				<div class="bliss-textimonial-author"><div class="border-one"></div><div class="border-two"></div><div class="auth-img"><?php echo wp_get_attachment_image( $testimonial_author_image, 'bliss-auth' ); ?></div></div>
			<?php } ?>
			<div class="bliss-testimonial-text"><?php the_sub_field( 'testimonial_author_text' ); ?></div>
			</div>
			</div>
	</section>
		
		<?php elseif ( get_row_layout() == 'pull_quote_section' ) : ?>
		<section class="pull-quote">
		<div class="container">
			<?php the_sub_field( 'pull_quote' ); ?>
			</div>
	</section>
	
	<?php elseif ( get_row_layout() == 'text_photo_montage_section' ) : ?>
		<section class="text-photo-montage">
		<div class="container">
		<div class="row">
		<div class="seven columns">
			<?php the_sub_field( 'text_column' ); ?>
			</div>
			<div class="five columns">
			<?php if ( have_rows( 'photo_montage' ) ): ?>
				<?php while ( have_rows( 'photo_montage' ) ) : the_row(); ?>
					<?php if ( get_row_layout() == 'two_photos' ) : ?>
						<?php $before_after_photo_one = get_sub_field( 'before_after_photo_one' ); ?>
						<?php if ( $before_after_photo_one ) { ?>
				<div class="one-half"><?php echo wp_get_attachment_image( $before_after_photo_one, 'body_soul_two' ); ?></div>
						<?php } ?>
						<?php $before_after_photo_two = get_sub_field( 'before_after_photo_two' ); ?>
						<?php if ( $before_after_photo_two ) { ?>
				<div class="one-half last"><?php echo wp_get_attachment_image( $before_after_photo_two, 'body_soul_two' ); ?></div>
						<?php } ?>
					<?php elseif ( get_row_layout() == 'one_photo' ) : ?>
						<?php $one_photo = get_sub_field( 'one_photo' ); ?>
						<?php if ( $one_photo ) { ?>
				<div class="full-width"><?php echo wp_get_attachment_image( $one_photo, 'body_soul_one' ); ?></div>
						<?php } ?>
						
				
		
					<?php endif; ?>
				<?php endwhile; ?>
			<?php else: ?>
				<?php // no layouts found ?>
			</div>
			
			<?php endif; ?>
			</div>
	</section>
		<?php elseif ( get_row_layout() == 'program_details_section' ) : ?>
			<?php $background_image = get_sub_field( 'background_image' ); ?>
			<?php if ( $background_image ) { ?>
			<section class="program-details" style="background-image:linear-gradient(rgba(255,255,255,.16), rgba(255,255,255,.16)),url(<?php echo $background_image['url']; ?>);">
			<div class="program-panel"><div class="program-panel-inner"><?php the_sub_field( 'program_details' ); ?></div></div>	
	</section>
			<?php } ?>
		<?php endif; ?>
	<?php endwhile; ?>
<?php else: ?>
	<?php // no layouts found ?>
<?php endif; ?>


</article>
<?php get_footer(); ?>