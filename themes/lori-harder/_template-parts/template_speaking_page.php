<?php
/*
Template Name: Speaking Page
*/
?>
<?php get_header(); ?>

<article>

<?php //Hero Header
	if(get_field('header') == "true") { 
	
	//var
	$hero_ht = get_field('hero_header_height');
	$bg_hero_img = get_field('header_image');
	$bg_hero_tint = get_field('header_tint');
	$bg_hero_text = get_field('header_text');	
?>

<!-- START HERO HEADER -->
<section class="hero-header" style="background-image:linear-gradient(rgba(0,0,0, <?php echo $bg_hero_tint ?>), rgba(0,0,0, <?php echo $bg_hero_tint ?>)),url(<?php echo $bg_hero_img['url']; ?>);min-height:<?php echo $hero_ht; ?>">
	<div class="container">
		<div class="hero-text-outer" style="height:<?php echo $hero_ht; ?>;<?php echo the_field('header_text_width_position_hero') ?>">
			<div class="hero-text"><?php echo $bg_hero_text; ?></div>
		</div>
	</div>
</section>
<!-- END HERO HEADER -->
<?php } ?>


<!-- VIDEO SECTION -->
<section class="speaking-video-section">
	<div class="container">
		<div class="row">
			<div class="ten columns">
				<?php 	//var
				$vid_id = get_field( 'youtube_video_id' );
				$attachment_id = get_field('video_image');
				$size = "full"; // (thumbnail, medium, large, full or custom size)
				$vid_image = wp_get_attachment_image_src( $attachment_id, $size );
				// url = $image[0];
				// width = $image[1];
				// height = $image[2];
				?>
				<div class="video-container">
    				<div class="video">
    					<img src="<?php echo $vid_image[0]; ?>">
    					<!-- <iframe width="987" height="508" src="http://www.youtube.com/embed/<?php echo $vid_id; ?>?autoplay=1" frameborder="0" allowfullscreen></iframe>-->
    				</div>
    			</div>				
			</div>
		</div>
	</div>
</section>

<!-- HIRE ME SECTION -->
<section class="hire-me">
	<div class="container">
		<div class="row">
			<?php the_field( 'hire_me_text' ); ?>
		</div>
	</div>
</section>

<!-- FEATURED IN SECTION -->	
<section class="featured-in">
	<div class="container">
		<div class="row">	
		<?php if ( have_rows( 'logos' ) ) : ?>
			<ul><span class="featured-in-text"><?php the_field( 'featured_in_text' ); ?></span>
			<?php while ( have_rows( 'logos' ) ) : the_row(); ?>
			<?php $logo = get_sub_field( 'logo' ); ?>
			<?php if ( $logo ) { ?>
				<li><img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" /></li>
			<?php } ?>
			<?php the_sub_field( 'logo_link' ); ?>
		<?php endwhile; ?>
		<?php else : ?>
		<?php // no rows found ?>
		<?php endif; ?>
			</ul>
		</div>
	</div>
</section>

<!-- SUGGESTED TOPICS SECTION -->		
<section class="suggested-topics">
	<div class="container">
		<h4><?php the_field( 'suggested_header_text' ); ?></h4>
		<div class="row">
		<?php if ( have_rows( 'features' ) ) : $count=0; ?>
		<?php while ( have_rows( 'features' ) ) : the_row(); ?>
			<div class="six columns">
				<a href="<?php the_sub_field( 'feature_page_link' ); ?>" class="feature-block-link"><div class="feature-block <?php echo ++$count%2 ? "odd" : "even" ?>"><?php the_sub_field( 'feature_title' ); ?></div></a>
			</div>
		<?php endwhile; ?>
		<?php else : ?>
			<?php // no rows found ?>
		<?php endif; ?>
		</div>
	
	<?php if ( have_rows( 'featured_images_row' ) ) : ?>
	<div class="row image-collage">
	<?php while ( have_rows( 'featured_images_row' ) ) : the_row(); ?>
		<?php $feature_image = get_sub_field( 'feature_image' ); ?>
		<div class="four columns">
		<?php if ( $feature_image ) { ?>
			<?php echo wp_get_attachment_image( $feature_image, 'speak-collage' ); ?>
		<?php } ?>
		</div>
	<?php endwhile; ?>
<?php else : ?>
	<?php // no rows found ?>
<?php endif; ?>
		</div>
	</div>
</section>

<!-- INQUIRE SECTION -->		
<section id="inquire" class="inquire">
	<div class="container">
		<div class="row">
			<?php the_field( 'inquire_header_text' ); ?>
			<?php 
				$form = get_field( 'contact_form_shortcode' ); 
				echo do_shortcode(''.$form.'');
			?>
		</div>
	</div>
</section>

</article>
<?php get_footer(); ?>