<?php
/**
 * Template part for displaying posts.
 *
 */
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="post-body entry-content">
    <h4 class="single-post-title"><?php the_title(); ?></h4>
    <div class="row">
    <div class="four columns">
    	<div class="featured blog"><?php the_post_thumbnail('full'); ?></div>
		</div>
       <div class="eight columns">
       <div class="additional-content">
       <?php $episode = get_field( 'in_the_episode' ); ?>
       <?php $resources = get_field( 'resources' ); ?>	
         
       <?php if( !empty($episode) ): ?>
		   <h4 class="episode-text-heading">In the episode you will here about:</h4>
		   <?php echo $episode; ?>
		   <br />
	   <?php endif; ?>
   
   	   <?php if( !empty($resources) ): ?>
		   <h4 class="episode-text-heading"><strong>Resources:</strong></h4>
		   <?php echo $resources; ?>
	   <?php endif; ?>
		   </div>
   	 </div>
		</div>
    <br />
		<h4 class="episode-text-heading">Show Notes</h4>
		<div class="post-content"><?php the_content(); ?></div>

<div class="post-footer">
	<div class="share-buttons">
     <?php 
		$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
		$subject =  get_the_title();
		$body = get_permalink();
		$desc = get_the_excerpt();
	?>
		<span class="share-title">SHARE</span>
       <div class="fshare"><a href="https://www.facebook.com/dialog/feed?app_id=184683071273&link=<?php the_permalink() ?>&picture=<?php echo $feat_image; ?>&name=<?php echo the_title (); ?>&caption=%20&description=<?php echo $desc; ?>&redirect_uri=http%3A%2F%2Fwww.facebook.com%2F" target="_blank"><i class="fa fa-facebook"></i></a></div>
       
       <div class="tshare"><a href="http://twitter.com/home?status=Currently reading: <?php the_title ();?><?php echo get_settings('home'); ?>/?p=<?php the_ID(); ?>" target="_blank"><i class="fa fa-twitter"></i></a></div>

      
    </div>
</div>

</div>

</div> <!--POST-->
