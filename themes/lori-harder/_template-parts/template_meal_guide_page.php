<?php
/*
Template Name: Meal Guides Page
*/
?>
<?php get_header(); ?>

<article>

<?php //Hero Header
	if(get_field('header') == "true") { 
	
	//var
	$hero_ht = get_field('hero_header_height');
	$bg_hero_img = get_field('header_image');
	$bg_hero_tint = get_field('header_tint');
	$bg_hero_text = get_field('header_text');	
?>

<!-- START HERO HEADER -->
<section class="hero-header" style="background-image:linear-gradient(rgba(0,0,0, <?php echo $bg_hero_tint ?>), rgba(0,0,0, <?php echo $bg_hero_tint ?>)),url(<?php echo $bg_hero_img['url']; ?>);min-height:<?php echo $hero_ht; ?>">
	<div class="container">
		<div class="hero-text-outer" style="height:<?php echo $hero_ht; ?>;<?php echo the_field('header_text_width_position_hero') ?>">
			<div class="hero-text"><?php echo $bg_hero_text; ?></div>
		</div>
	</div>
</section>
<!-- END HERO HEADER -->
<?php } ?>


<section class="meal-guide-content">
<div class="container">
<div class="row">
<div class="eight columns">
<h1><?php the_title(); ?></h1>
<?php the_field( 'meal_guide_text' ); ?>
	</div>
	<div class="four columns">
<?php $meal_guide_image = get_field( 'meal_guide_image' ); ?>
<?php if ( $meal_guide_image ) { ?>
	<img src="<?php echo $meal_guide_image['url']; ?>" alt="<?php echo $meal_guide_image['alt']; ?>" />
<?php } ?>
	</div>
	</div>
	</div>
	</section>	

<?php if ( have_rows( 'meal_guides' ) ) : ?>
<section class="meal-guides">
<div class="container">
<div class="row">
	<?php while ( have_rows( 'meal_guides' ) ) : the_row(); ?>
	<div class="four columns">
		<?php $meal_guide_image = get_sub_field( 'meal_guide_image' ); ?>
		<?php if ( $meal_guide_image ) { ?>
			<a href="<?php the_sub_field( 'meal_guide_button_link' ); ?>" target="_blank"><?php echo wp_get_attachment_image( $meal_guide_image, 'meal-guide' ); ?>
		<?php } ?>
				<div class="button button-primary" ><?php the_sub_field( 'meal_guide_button_label' ); ?></div></a>
	</div>
	<?php endwhile; ?>
<?php else : ?>
	<?php // no rows found ?>
<?php endif; ?>
	</div>
	</div>
</section>
</article>
<?php get_footer(); ?>