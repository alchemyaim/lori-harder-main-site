<?php
/*
Template Name: Book Page
*/
?>
<?php get_header(); ?>
<style>.hero-header {margin: 0 auto -115px;}.instashow-gallery{display:none;}</style>
<article>   

<?php //Hero Header
	if(get_field('header') == "true") { 
	
	//var
	$hero_ht = get_field('hero_header_height');
	$bg_hero_img = get_field('header_image');
	$bg_hero_tint = get_field('header_tint');
	$bg_hero_text = get_field('header_text');	
?>

<!-- START HERO HEADER -->
<section class="hero-header" style="background-image:linear-gradient(rgba(0,0,0, <?php echo $bg_hero_tint ?>), rgba(0,0,0, <?php echo $bg_hero_tint ?>)),url(<?php echo $bg_hero_img['url']; ?>);min-height:<?php echo $hero_ht; ?>">
	<div class="container">
		<div class="hero-text-outer" style="height:<?php echo $hero_ht; ?>;<?php echo the_field('header_text_width_position_hero') ?>">
			<div class="hero-text"><?php echo $bg_hero_text; ?></div>
		</div>
	</div>
</section>
<!-- END HERO HEADER -->
<?php } ?>


<!-- VIDEO SECTION -->
<section class="intro-video-section">
<div class="container">
	<h1><?php the_title(); ?></h1>
<div class="row">
<div class="twelve columns">
	<?php //var
		$vid_id = get_field( 'youtube_video_id' );
		$attachment_id = get_field('video_image');
			$size = "full"; // (thumbnail, medium, large, full or custom size)
    		$vid_image = wp_get_attachment_image_src( $attachment_id, $size );
			// url = $image[0];
    		// width = $image[1];
    		// height = $image[2];
		?>
		<div class="video-container">
    		<div class="video">
    			<img src="<?php echo $vid_image[0]; ?>">
    				<!-- <iframe width="987" height="508" src="http://www.youtube.com/embed/<?php echo $vid_id; ?>?autoplay=1" frameborder="0" allowfullscreen></iframe>-->
    				
    				<script>
	jQuery(function($) {
        "use strict";
	var newHtml = '<iframe width="987" height="508" src="http://www.youtube.com/embed/<?php echo $vid_id; ?>?autoplay=1" frameborder="0" allowfullscreen></iframe>';
	</script>
   		
    		</div>
    	</div>				
	</div>
	</div>
	</div>
	</section>
	
<!-- OPT IN SECTION -->
<section class="book-optin">
<div class="container">
<div class="row">
<div class="twelve columns">
<?php the_field( 'opt_in_header' ); ?>
	</div>
	</div>
	</div>
	</section>

<!-- A TRIBE CALLED BLISS -->
<section class="tribe-bliss-section">
<div class="container">
<div class="row">
	<div class="five columns">
	<div class="book-img">
	<?php $column_one_image = get_field( 'column_one_image' ); ?>
<?php if ( $column_one_image ) { ?>
	<?php echo wp_get_attachment_image( $column_one_image, 'full' ); ?>
<?php } ?>
		<ul class="book-logos">

<?php if ( have_rows( 'column_one_logos' ) ) : ?>
	<?php while ( have_rows( 'column_one_logos' ) ) : the_row(); ?>
		<?php 
			//var
			$logo = get_sub_field( 'logo' ); 
			$link = get_sub_field( 'logo_link' ); 
		?>
		<li>
		<?php if( $link ): ?>
				<a href="<?php echo $link; ?>">
			<?php endif; ?>
			
		<?php if ( $logo ) { ?>
			<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
		<?php } ?>
		
		<?php if( $link ): ?>
				</a>
			<?php endif; ?>
			</li>
	
	<?php endwhile; ?>
<?php else : ?>
	<?php // no rows found ?>
<?php endif; ?>
</ul>
	</div>
	</div>
	<div class="seven columns">
	<div class="tribe-text">
	<?php the_field( 'column_two_text' ); ?>
		</div>
	</div>
	</div>
	</div>
	</section>

<!-- DOWNLOAD A CHAPTER -->
<section class="chapter-section">
<div class="container">
<div class="row">
<div class="five columns">
<div class="download-img">
	<?php $download_image = get_field( 'download_image' ); ?>
<?php if ( $download_image ) { ?>
	<img src="<?php echo $download_image['url']; ?>" alt="<?php echo $download_image['alt']; ?>" />
<?php } ?>
	</div>
	</div>
	<div class="seven columns">
	<div class="download-text">
	<?php the_field( 'download_text' ); ?>
		</div>
	</div>
	</div>
	</div>
	</section>

</article>
<?php get_footer(); ?>