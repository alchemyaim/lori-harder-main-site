<?php
/*
Template Name: Contact Page
*/
?>
<?php get_header(); ?>
<article>   
	<style>.hero-header{margin: 0 auto -100px; }</style>
<?php //Hero Header
	if(get_field('header') == "true") { 
	
	//var
	$hero_ht = get_field('hero_header_height');
	$bg_hero_img = get_field('header_image');
	$bg_hero_tint = get_field('header_tint');
	$bg_hero_text = get_field('header_text');	
?>

<!-- START HERO HEADER -->
<section class="hero-header" style="background-image:linear-gradient(rgba(0,0,0, <?php echo $bg_hero_tint ?>), rgba(0,0,0, <?php echo $bg_hero_tint ?>)),url(<?php echo $bg_hero_img['url']; ?>);min-height:<?php echo $hero_ht; ?>">
	<div class="container">
		<div class="hero-text-outer" style="height:<?php echo $hero_ht; ?>;<?php echo the_field('header_text_width_position_hero') ?>">
			<div class="hero-text"><?php echo $bg_hero_text; ?></div>
		</div>
	</div>
</section>
<!-- END HERO HEADER -->
<?php } ?>


<!-- INTRO SECTION -->
<section class="contact-intro-section">
	<div class="container">
		<div class="row">
			<div class="eight columns">
				<?php the_field( 'contact_intro_text' ); ?>
			</div>
			<div class="two columns contact-social-links">
				<?php if(is_active_sidebar('menu_social_links')) dynamic_sidebar('menu_social_links'); ?>
			</div>
		</div>
		<div class="row">
			<div class="twelve columns contact-forms">
			<?php
				//var
				$tab_one = get_field( 'first_form_tab' );
				$tab_two = get_field( 'second_form_tab' );
				$tab_three = get_field( 'third_form_tab' );
				$form_one = get_field( 'first_contact_form_shortcode' );
				$form_two = get_field( 'second_contact_form_shortcode' );
				$form_three = get_field( 'third_contact_form_shortcode' );
				?>
				
				
				<div class="tab">
				  <button class="tablinks" onclick="openForm(event, 'tab-one')" id="defaultOpen"><?php echo do_shortcode(''.$tab_one.''); ?></button>
				  <button class="tablinks" onclick="openForm(event, 'tab-two')"><?php echo do_shortcode(''.$tab_two.''); ?></button>
				  <button class="tablinks" onclick="openForm(event, 'tab-three')"><?php echo do_shortcode(''.$tab_three.''); ?></button>
				</div>

<div id="tab-one" class="tabcontent">
  <?php echo do_shortcode(''.$form_one.''); ?>
</div>

<div id="tab-two" class="tabcontent">
  <?php echo do_shortcode(''.$form_two.''); ?>
</div>

<div id="tab-three" class="tabcontent">
  <?php echo do_shortcode(''.$form_three.''); ?>
</div>
				
<script>
	function openForm(evt, cityName) {
		var i, tabcontent, tablinks;
		tabcontent = document.getElementsByClassName("tabcontent");
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(cityName).style.display = "block";
		evt.currentTarget.className += " active";
	}

	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();
</script>
		
					
				
			</div>
		</div>
	</div>
</section>
	
</article>
<?php get_footer(); ?>