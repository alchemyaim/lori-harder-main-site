<?php
/*
Template Name: Home Page
*/
?>
<?php get_header(); ?>
<article>   

<?php //Hero Header
	if(get_field('header') == "true") { 
	
	//var
	$hero_ht = get_field('hero_header_height');
	$bg_hero_img = get_field('header_image');
	$bg_hero_tint = get_field('header_tint');
	$bg_hero_text = get_field('header_text');	
?>

<!-- START HERO HEADER -->
<section class="hero-header" style="background-image:linear-gradient(rgba(0,0,0, <?php echo $bg_hero_tint ?>), rgba(0,0,0, <?php echo $bg_hero_tint ?>)),url(<?php echo $bg_hero_img['url']; ?>);min-height:<?php echo $hero_ht; ?>">
	<div class="container">
		<div class="hero-text-outer" style="height:<?php echo $hero_ht; ?>;<?php echo the_field('header_text_width_position_hero') ?>">
			<div class="hero-text"><?php echo $bg_hero_text; ?></div>
		</div>
	</div>
</section>
<!-- END HERO HEADER -->
<?php } ?>


<!-- VIDEO SECTION -->
<section class="video-section">
<div class="container">
<div class="row">
<div class="four columns vid-text">
<?php the_field( 'text_column' ); ?>


	</div>
	<div class="eight columns vid">
	<?php //var
		$vid_id = get_field( 'youtube_video_id' );
		$attachment_id = get_field('video_image');
			$size = "vid-placeholder"; // (thumbnail, medium, large, full or custom size)
    		$vid_image = wp_get_attachment_image_src( $attachment_id, $size );
			// url = $image[0];
    		// width = $image[1];
    		// height = $image[2];
		?>
		<div class="video-container">
    		<div class="video">
    			<img src="<?php echo $vid_image[0]; ?>">
    				<!-- <iframe width="987" height="508" src="http://www.youtube.com/embed/<?php echo $vid_id; ?>?autoplay=1" frameborder="0" allowfullscreen></iframe>-->
    				
    				<script>
	jQuery(function($) {
        "use strict";
	var newHtml = '<iframe width="987" height="508" src="http://www.youtube.com/embed/<?php echo $vid_id; ?>?autoplay=1" frameborder="0" allowfullscreen></iframe>';

$(".play-vid").click(function(e) {
    //Get rid of that old HTML content
    $(".video").empty();
	$(".video").addClass('player');
    
    //Bring in the new content!
    $(".video").append(newHtml);

	
	e.preventDefault();
});
		});
	
	</script>
   		
    		</div>
    	</div>				
	</div>
	</div>
	</div>
	</section>
	
	
	<!-- QUICK LINKS SECTION -->
	<section class="quick-links">
	<div class="container">
	<div class="row">
	<?php if ( have_rows( 'quick_links' ) ) : ?>
	<?php while ( have_rows( 'quick_links' ) ) : the_row(); ?>
		<?php $callout_image = get_sub_field( 'callout_image' ); ?>
		<div class="four columns">
		<figure class="effect-chico">
						<?php if ( $callout_image ) { ?>
			<?php echo wp_get_attachment_image( $callout_image, 'quicklinks' ); ?>
		<?php } ?>
						<figcaption onclick="location.href='<?php the_sub_field( 'callout_link' ); ?>'">
							<h2><?php the_sub_field( 'callout_title' ); ?></h2>
							<p><a href="<?php the_sub_field( 'callout_link' ); ?>"><?php the_sub_field( 'callout_subtitle' ); ?></a></p>
						</figcaption>			
					</figure>
		</div>
		
	<?php endwhile; ?>
<?php else : ?>
	<?php // no rows found ?>
<?php endif; ?>
		</div>
		</div>
	</section>

<!-- POPULAR PODCASTS -->
<section class="podcasts">
<div class="container">
<div class="row">
<div class="twelve columns">
	<h2 class="poppod-header"><?php the_field( 'popular_podcasts_title' ); ?></h2>
	</div>
	</div>
	</div>
<div class="row">
<?php if ( have_rows( 'podcasts' ) ) : ?>
	<?php while ( have_rows( 'podcasts' ) ) : the_row(); ?>
		<?php //var
			$attachment_id = get_sub_field('podcast_image');
			$size = "poppods"; // (thumbnail, medium, large, full or custom size)
    		$podcast_image = wp_get_attachment_image_src( $attachment_id, $size );
			// url = $image[0];
    		// width = $image[1];
    		// height = $image[2];
		?>
		<a href="<?php the_sub_field( 'podcast_page_link' ); ?>">
		<div class="podcast" style="background-image:url(<?php echo $podcast_image[0]; ?>);">
			<div class="pod-title"><?php the_sub_field( 'podcast_title' ); ?></div>
			</div>
	</a>
		
	<?php endwhile; ?>
<?php else : ?>
	<?php // no rows found ?>
<?php endif; ?>
	</div>
	</section>
	<div class="clear-fix"></div>
<!-- OPT IN SECTION -->
<section class="optin">
<div class="container">
<div class="row">
<div class="twelve columns">
<?php the_field( 'opt_in_header' ); ?>
<?php the_field( 'form_code', 'option' ); ?>
<script>jQuery(document).ready(function($){ $("#mr-field-element-195998811846").prop('value', 'join your tribe'); });</script>
	</div>
	</div>
	</div>
	</section>

<!-- LATEST HAPPENINGS -->
<section class="happenings-section">
<div class="container">
<?php //var
	$attachment_id = get_field('background_image');
	$size = "full"; // (thumbnail, medium, large, full or custom size)
    $bg_image = wp_get_attachment_image_src( $attachment_id, $size );
	// url = $image[0];
    // width = $image[1];
    // height = $image[2];
	?>

<div class="happenings-container" style="background-image:url(<?php echo $bg_image[0]; ?>);">
<div class="happenings-panel">
	<h3><?php the_field( 'latest_header_text' ); ?></h3>
<?php if ( have_rows( 'happenings' ) ) : ?>
<ul>
	<?php while ( have_rows( 'happenings' ) ) : the_row(); ?>
	<li>
		<div class="event-title"><?php the_sub_field( 'event_title' ); ?></div>
		<div class="event-date"><?php the_sub_field( 'event_date' ); ?></div>
		<a href="<?php the_sub_field( 'event_link' ); ?>">Learn More</a>
	</li>
	<?php endwhile; ?>
<?php else : ?>
	<?php // no rows found ?>
<?php endif; ?>
	</ul>
	</div>
	</div>
	</section>

<!-- SOCIAL STORIES TITLE -->
<section class="social-stories">
<div class="container">
<div class="row">
<div class="five columns social-text">
<div class="ss-text">
	<div class="social-title"><?php the_field( 'social_stories_title' ); ?></div>
	<?php the_field( 'social_stories_text' ); ?>
	</div>
	</div>
	<div class="seven columns social-img">
<?php $social_stories_image = get_field( 'social_stories_image' ); ?>
<?php if ( $social_stories_image ) { ?>
	<?php echo wp_get_attachment_image( $social_stories_image, 'full' ); ?>
<?php } ?>
	</div>
	</div>
	</div>
	</section>

</article>
<?php get_footer(); ?>