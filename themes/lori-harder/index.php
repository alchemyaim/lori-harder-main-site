<?php
/**
 * Index Page
 * 
 */

get_header();
?>
<article>

<?php //Hero Header
	if(get_field('header','option') == "true") { 
	
	//var
	$hero_ht = get_field('hero_header_height', 'option');
	$bg_hero_img = get_field('header_image', 'option');
	$bg_hero_tint = get_field('hero_header_tint', 'option');
	$bg_hero_text = get_field('header_text', 'option');	
?>

<!-- START HERO HEADER -->
<section class="hero-header blog-header" style="background-image:linear-gradient(rgba(0,0,0, <?php echo $bg_hero_tint ?>), rgba(0,0,0, <?php echo $bg_hero_tint ?>)),url(<?php echo $bg_hero_img['url']; ?>);min-height:<?php echo $hero_ht; ?>">
	<div class="container">
		<div class="hero-text-outer" style="height:<?php echo $hero_ht; ?>;<?php echo the_field('header_text_width_position_hero', 'option') ?>">
			<div class="hero-text"><?php echo $bg_hero_text; ?></div>
		</div>
	</div>
</section>
<!-- END HERO HEADER -->
<?php } ?>

	<div class="container">
		<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>

				<!-- Display All Posts -->
<div id="post-<?php the_ID(); ?>" <?php post_class('post clearfix post-list'); ?>>
	<div class="row">
		<div class="four columns">
			<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('blog-posts'); ?></a>
		</div>
		<div class="seven columns offset-by-one">
 			<div class="post-text">
  			<a class="post-title-link" href="<?php the_permalink(); ?>"><h4 class="post-title"><?php the_title(); ?></h4></a>
  				<?php the_excerpt(__('new_excerpt_length')); ?>
    				<div class="post-meta"><a href="<?php the_permalink(); ?>">LISTEN HERE</a> <!--&ensp;|&ensp; <!--?php comments_number(__('Comments 0')); ?--></div>	
			</div>	
		</div>
	</div>
</div>

			<?php endwhile; ?>
           
            <?php get_template_part( '_template-parts/part', 'navigation' ); ?>

		<?php else : ?>

			<h1 class="heading">Not Found</h1>
			<p class="center">Sorry, but you are looking for something that isn't here.</p>
			<?php get_search_form(); ?>

		<?php endif; ?>
	</div>
</article>
<?php get_footer(); ?>
