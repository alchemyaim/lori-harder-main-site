<?php
/**
 * The loop
 */

global $more
?>

<?php //If there are no posts to display, such as an empty archive page
	if ( ! have_posts() ) : ?>
		<h1>There are currently no entries for this archive.</h1>
		<?php get_search_form(); ?>
<?php endif; ?>
<?php while ( have_posts() ) : the_post(); ?>

<!-- Display All Posts -->
<div id="post-<?php the_ID(); ?>" <?php post_class('post clearfix post-list'); ?>>
	<div class="row">
		<div class="four columns">
			<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('blog-posts'); ?></a>
		</div>
		<div class="seven columns offset-by-one">
 			<div class="post-text">
  			<a class="post-title-link" href="<?php the_permalink(); ?>"><h4 class="post-title"><?php the_title(); ?></h4></a>
  				<?php the_excerpt(__('new_excerpt_length')); ?>
    				<div class="post-meta"><a href="<?php the_permalink(); ?>">LISTEN HERE</a> <!--&ensp;|&ensp; <!--?php comments_number(__('Comments 0')); ?--></div>	
			</div>	
		</div>
	</div>
</div>
<?php endwhile;
wp_reset_query();
?>