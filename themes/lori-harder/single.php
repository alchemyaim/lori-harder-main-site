<?php
/**
 * The template for displaying all single posts.
 */

get_header();
?>

<article>
<?php 
	
	/**************************************************
	****** IF THIS IS UNDER THE CATEGORY PODCAST ******
	**************************************************/
	
if (in_category('podcast')){ ?>

	<style>.instashow-gallery{display:none;}</style>

<?php //Hero Header
	if(get_field('header','option') == "true") { 
	
	//var
	$hero_ht = get_field('hero_header_height', 'option');
	$bg_hero_img = get_field('header_image', 'option');
	$bg_hero_tint = get_field('hero_header_tint', 'option');
	$bg_hero_text = get_field('header_text', 'option');	
?>

<!-- START HERO HEADER -->
<!--section class="hero-header blog-header" style="background-image:linear-gradient(rgba(0,0,0, <?php echo $bg_hero_tint ?>), rgba(0,0,0, <?php echo $bg_hero_tint ?>)),url(<?php echo $bg_hero_img['url']; ?>);min-height:<?php echo $hero_ht; ?>">
	<div class="container">
		<div class="hero-text-outer" style="height:<?php echo $hero_ht; ?>;<?php echo the_field('header_text_width_position_hero', 'option') ?>">
			<div class="hero-text"><?php echo $bg_hero_text; ?></div>
		</div>
	</div>
</section-->
<!-- END HERO HEADER -->
<?php } ?>

<div class="container">
	<?php
    while ( have_posts() ) : the_post();
    get_template_part( '_template-parts/content', get_post_format() ); ?>
	
	<section class="subscribe-links">
		<div class="contianer">
			<div class="row">
  				<?php if ( have_rows( 'subscribe_buttons', 'option' ) ) : ?>
				<div class="subscribe-buttons">
					<div class="subscribe-text">Subscribe</div>
						<?php while ( have_rows( 'subscribe_buttons', 'option' ) ) : the_row(); ?>
						<a href="<?php the_sub_field( 'button_link' ); ?>" class="button" target="_blank"><?php the_sub_field( 'button_label' ); ?></a>
						<?php endwhile; ?>
				</div>
						<?php else : ?>
							<?php // no rows found ?>
				<?php endif; ?>
			</div>
		</div>
	</section>	
<?php //RELATED POSTS	
$orig_post = $post;
global $post;
$categories = get_the_category($post->ID);
if ($categories) {
$category_ids = array();
foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
$args=array(
'category__in' => $category_ids,
'post__not_in' => array($post->ID),
'posts_per_page'=> 3, // Number of related posts that will be shown.
'caller_get_posts'=>1
);
$my_query = new wp_query( $args );
if( $my_query->have_posts() ) {
echo '<div id="related_posts"><h3>You may also like...</h3><div class="row">';
while( $my_query->have_posts() ) {
$my_query->the_post();?>
    <div class="four columns">
      	<div class="pop-post-img" onclick="location.href='<?php the_permalink(); ?>'">
			<?php the_post_thumbnail('full'); ?>
			<div class="overlay"><div class="post-title remove-colon"><?php the_title(); ?></div><h1 class="listen-text">Listen Now</h1><div class="play-btn"></div></div>
		</div>
    </div>
    <?
}
echo '</div></div>';
}
}
$post = $orig_post;
wp_reset_query(); 
get_template_part( '_template-parts/part', 'navigation' );
endwhile; // End of the loop. ?>
</div>

<?php 
	
	/**************************************************
	*********   IF THIS IS UNDER THE RECIPES  *********
	**************************************************/

} elseif (in_category('recipes')) { ?>
<?php //Hero Header
		
	//var
	$r_img = get_field('recipes_header_image', 'option');
	$r_tnt = get_field('recipes_header_tint', 'option');
	$r_txt = get_field('recipes_header_text', 'option');
		
	?>
	<!-- START HERO HEADER -->
	<section class="hero-header" style="background-image:linear-gradient(rgba(0,0,0,<?php echo $r_tnt ?>), rgba(0,0,0,<?php echo $r_tnt ?>)),url(<?php echo $r_img['url']; ?>);min-height:750px">
		<div class="container">
			<div class="hero-text-outer" style="height:750px;max-width:100%;">
				<div class="hero-text"><?php echo $r_txt ?></div>
			</div>
		</div>
	</section>
	<!-- END HERO HEADER -->

<div class="recipe-posts container">
	<div class="blog-container">
		<div class="content-area">
	<?php while ( have_posts() ) : the_post(); ?>
       		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="post-body entry-content">
    				<h4 class="single-post-title"><?php the_title(); ?></h4>
					<div class="post-content"><?php the_content(); ?></div>
					<!--?php echo do_shortcode('[fbcomments]'); ?-->
				</div>
			</div>
	<?php endwhile; ?>
	</div>
</div>
<?php get_sidebar(); ?>
	<div class="post-footer center">
		<div class="share-buttons">
		 <?php 
			$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
			$subject =  get_the_title();
			$body = get_permalink();
			$desc = get_the_excerpt();
		?>
			<span class="share-title">SHARE</span>
		   <div class="fshare"><a href="https://www.facebook.com/dialog/feed?app_id=184683071273&link=<?php the_permalink() ?>&picture=<?php echo $feat_image; ?>&name=<?php echo the_title (); ?>&caption=%20&description=<?php echo $desc; ?>&redirect_uri=http%3A%2F%2Fwww.facebook.com%2F" target="_blank"><i class="fa fa-facebook"></i></a></div>

		   <div class="tshare"><a href="http://twitter.com/home?status=Currently reading: <?php the_title ();?><?php echo get_settings('home'); ?>/?p=<?php the_ID(); ?>" target="_blank"><i class="fa fa-twitter"></i></a></div>
		</div>
	</div>
</div>
<?php 

	/**************************************************
	******   IF THIS IS UNDER THE WORKOUT VIDEO  ******
	**************************************************/
								   
} elseif (in_category('workout-videos')) { ?>
<?php //Hero Header
		
	//var
	$w_img = get_field('workout_videos_header_image', 'option');
	$w_tnt = get_field('workout_videos_header_tint', 'option');
	$w_txt = get_field('workout_videos_header_text', 'option');
		
	?>
	<!-- START HERO HEADER -->
	<!--section class="hero-header" style="background-image:linear-gradient(rgba(0,0,0,<?php echo $w_tnt ?>), rgba(0,0,0,<?php echo $w_tnt ?>)),url(<?php echo $w_img['url']; ?>);min-height:750px">
		<div class="container">
			<div class="hero-text-outer" style="height:750px;max-width:100%;">
				<div class="hero-text"><?php echo $w_txt ?></div>
			</div>
		</div>
	</section>
	<!-- END HERO HEADER -->
	
<div class="container workout-vlog-vids">
	<?php while ( have_posts() ) : the_post(); ?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    	<div class="post-body entry-content">
    		<h1 class="post-title vid-title"><?php the_title(); ?></h1>
			<div class="post-content"><?php the_content(); ?></div>
		</div>
	<div class="post-footer">
		<div class="share-buttons">
		 <?php 
			$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
			$subject =  get_the_title();
			$body = get_permalink();
			$desc = get_the_excerpt();
		?>
			<span class="share-title">SHARE</span>
		   <div class="fshare"><a href="https://www.facebook.com/dialog/feed?app_id=184683071273&link=<?php the_permalink() ?>&picture=<?php echo $feat_image; ?>&name=<?php echo the_title (); ?>&caption=%20&description=<?php echo $desc; ?>&redirect_uri=http%3A%2F%2Fwww.facebook.com%2F" target="_blank"><i class="fa fa-facebook"></i></a></div>

		   <div class="tshare"><a href="http://twitter.com/home?status=Currently reading: <?php the_title ();?><?php echo get_settings('home'); ?>/?p=<?php the_ID(); ?>" target="_blank"><i class="fa fa-twitter"></i></a></div>


		</div>
	</div>
	</div>	 
	<?php endwhile; ?>
</div>


<?php 

	/*************************************************
	************  IF THIS IS UNDER VLOGS  ************
	**************************************************/
								   
} elseif (in_category('healthy-minds')) { ?>
<?php //Hero Header
		
	//var
	$v_img = get_field('vlogs_header_image', 'option');
	$v_tnt = get_field('vlogs_header_tint', 'option');
	$v_txt = get_field('vlogs_header_text', 'option');
		
	?>
	<!-- START HERO HEADER -->
	<!--section class="hero-header" style="background-image:linear-gradient(rgba(0,0,0,<?php echo $v_tnt ?>), rgba(0,0,0,<?php echo $v_tnt ?>)),url(<?php echo $v_img['url']; ?>);min-height:750px">
		<div class="container">
			<div class="hero-text-outer" style="height:750px;max-width:100%;">
				<div class="hero-text"><?php echo $v_txt ?></div>
			</div>
		</div>
	</section>
	<!-- END HERO HEADER -->

<div class="container">
	<?php while ( have_posts() ) : the_post(); ?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    	<div class="post-body entry-content">
    		<h1 class="post-title vid-title"><?php the_title(); ?></h1>
			<div class="post-content"><?php the_content(); ?></div>
		</div>
	<div class="post-footer">
		<div class="share-buttons">
		 <?php 
			$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
			$subject =  get_the_title();
			$body = get_permalink();
			$desc = get_the_excerpt();
		?>
			<span class="share-title">SHARE</span>
		   <div class="fshare"><a href="https://www.facebook.com/dialog/feed?app_id=184683071273&link=<?php the_permalink() ?>&picture=<?php echo $feat_image; ?>&name=<?php echo the_title (); ?>&caption=%20&description=<?php echo $desc; ?>&redirect_uri=http%3A%2F%2Fwww.facebook.com%2F" target="_blank"><i class="fa fa-facebook"></i></a></div>

		   <div class="tshare"><a href="http://twitter.com/home?status=Currently reading: <?php the_title ();?><?php echo get_settings('home'); ?>/?p=<?php the_ID(); ?>" target="_blank"><i class="fa fa-twitter"></i></a></div>   
		</div>
	</div>
	</div>

			
<?php //RELATED POSTS	
$orig_post = $post;
global $post;
$categories = get_the_category($post->ID);
if ($categories) {
$category_ids = array();
foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
$args=array(
'category__in' => 9,
'post__not_in' => array($post->ID),
'posts_per_page'=> 3, // Number of related posts that will be shown.
'caller_get_posts'=>1
);
$my_query = new wp_query( $args );
if( $my_query->have_posts() ) {
echo '<div id="related_posts" class="related-vlogs"><h3>You may also like...</h3><div class="row">';
while( $my_query->have_posts() ) {
$my_query->the_post();?>
    <div class="four columns">
      	<div class="pop-post-img" onclick="location.href='<?php the_permalink(); ?>'">
			<?php the_post_thumbnail('vlog-posts'); ?>
			<div class="overlay"><div class="post-title remove-colon"><?php the_title(); ?></div><h1 class="listen-text">Watch Now</h1><div class="play-btn vlog-play"></div></div>
		</div>
    </div>
    <?
}
echo '</div></div>';
}
}
$post = $orig_post;
wp_reset_query(); 
	
	
$post_id = $post->ID; // current post ID
$cat = get_the_category(); 
$current_cat_id = $cat[0]->cat_ID; // current category ID 

$args = array( 
    'category' => 9,
    'orderby'  => 'post_date',
    'order'    => 'DESC'
);
$posts = get_posts( $args );
// get IDs of posts retrieved from get_posts
$ids = array();
foreach ( $posts as $thepost ) {
    $ids[] = $thepost->ID;
}
// get and echo previous and next post in the same category
$thisindex = array_search( $post_id, $ids );
$nextid = $ids[ $thisindex - 1 ];
$previd = $ids[ $thisindex + 1 ]; ?>

<div id="navigation">
	<div id="pagination">
<?php if ( ! empty( $previd ) ) {
		?><div class="nav-older"><a rel="prev" href="<?php echo get_permalink($previd) ?>" class="button button-primary">Previous Post</a></div><?php
}
if ( ! empty( $nextid ) ) {
		?><div class="nav-newer"><a rel="next" href="<?php echo get_permalink($nextid) ?>" class="button button-primary">Next Post</a></div><?php
} ?>
<div style="clear:both;"></div>
	</div>
</div>
<?php endwhile; ?>
</div>

<?php 
	
	/**************************************************
	******  IF THIS IS UNDER ANY OTHER CATEGORY ******
	**************************************************/									 
} else { ?>
<div class="container">
	<?php while ( have_posts() ) : the_post();
       	  get_template_part( '_template-parts/content', get_post_format() ); ?>
    <?php endwhile; ?>
</div>
<?php } ?>

</article>
<?php get_footer(); ?>