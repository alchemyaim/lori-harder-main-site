<?php

/*-----------------------------------------------------------------------------------*/
/*  CUSTOM POST TYPE REGISTRATION
/*-----------------------------------------------------------------------------------*/

// Creates Custom Post Type
function resources_init() {
    $args = array(
      'label' => 'Resources',
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'resources'),
        'query_var' => true,
        'menu_icon' => 'dashicons-universal-access',
        'supports' => array(
            'title',
            'editor',
            'revisions',
            'thumbnail',)
        );
    register_post_type( 'resources', $args );
}
add_action( 'init', 'resources_init' );

/*-----------------------------------------------------------------------------------*/
/*  CUSTOM INTERACTION MESSAGES (optional)
/*-----------------------------------------------------------------------------------*/

function resource_updated_messages( $messages ) {
  global $post, $post_ID;
  $messages['resources'] = array(
    0 => '', 
    1 => sprintf( __('Resource updated. <a href="%s"> View resource page.</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Resource updated.'),
    5 => isset($_GET['revision']) ? sprintf( __('Resource restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Resource published. <a href="%s">View resource page.</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Resource saved.'),
    8 => sprintf( __('Resource submitted. <a target="_blank" href="%s">Preview resource page</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Resource scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview resource page</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Resource draft updated. <a target="_blank" href="%s">Preview resource page</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}
add_filter( 'post_updated_messages', 'resource_updated_messages' );


/*-----------------------------------------------------------------------------------*/
/*  CONTEXTUAL HELP (optional)
/*-----------------------------------------------------------------------------------*/

function resource_contextual_help( $contextual_help, $screen_id, $screen ) { 
  if ( 'edit-resources' == $screen->id ) {

    $contextual_help = '<h2>Resources</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce est libero, congue eget gravida mollis, eleifend sit amet felis.</p> 
    <p>Sed mollis pretium dolor at vestibulum. Phasellus condimentum dui in velit interdum, sed aliquam ex hendrerit.</p>';

  } elseif ( 'resources' == $screen->id ) {

    $contextual_help = '<h2>Editing Resources</h2>
    <p>Nunc eleifend arcu sit amet tortor <strong>luctus aliquam.</strong> Sed a massa vestibulum, iaculis magna ac, faucibus tellus.</p>';

  }
  return $contextual_help;
}
add_action( 'contextual_help', 'resource_contextual_help', 10, 3 );

/* THIS HELPS FOR SINGLE PAGES FOR CUSTOM POST TYPES */
flush_rewrite_rules();
?>