<?php

/*-----------------------------------------------------------------------------------*/
/*  CUSTOM POST TYPE REGISTRATION
/*-----------------------------------------------------------------------------------*/

// Creates Custom Post Type
function programs_init() {
    $args = array(
      'label' => 'Programs',
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'programs'),
        'query_var' => true,
        'menu_icon' => 'dashicons-welcome-learn-more',
        'supports' => array(
            'title',
            'editor',
            'revisions',
            'thumbnail',)
        );
    register_post_type( 'programs', $args );
}
add_action( 'init', 'programs_init' );

/*-----------------------------------------------------------------------------------*/
/*  CUSTOM INTERACTION MESSAGES (optional)
/*-----------------------------------------------------------------------------------*/

function program_updated_messages( $messages ) {
  global $post, $post_ID;
  $messages['programs'] = array(
    0 => '', 
    1 => sprintf( __('Programupdated. <a href="%s"> View program page.</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Programupdated.'),
    5 => isset($_GET['revision']) ? sprintf( __('Programrestored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Programpublished. <a href="%s">View program page.</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Programsaved.'),
    8 => sprintf( __('Programsubmitted. <a target="_blank" href="%s">Preview program page</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Programscheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview program page</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Programdraft updated. <a target="_blank" href="%s">Preview program page</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}
add_filter( 'post_updated_messages', 'program_updated_messages' );


/* THIS HELPS FOR SINGLE PAGES FOR CUSTOM POST TYPES */
flush_rewrite_rules();
?>