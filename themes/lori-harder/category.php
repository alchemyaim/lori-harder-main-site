<?php
/**
 * Categories Page
 */

get_header();
?>
<article>

<?php
	
	/**************************************************
	****** IF THIS IS UNDER THE CATEGORY PODCAST ******
	**************************************************/
	
	if (in_category('podcast')){ ?>
<?php //Hero Header
	if(get_field('header','option') == "true") { 
	
	//var
	$hero_ht = get_field('hero_header_height', 'option');
	$bg_hero_img = get_field('header_image', 'option');
	$bg_hero_tint = get_field('hero_header_tint', 'option');
	$bg_hero_text = get_field('header_text', 'option');	
?>

<!-- START HERO HEADER -->
<section class="hero-header blog-header" style="background-image:linear-gradient(rgba(0,0,0, <?php echo $bg_hero_tint ?>), rgba(0,0,0, <?php echo $bg_hero_tint ?>)),url(<?php echo $bg_hero_img['url']; ?>);min-height:<?php echo $hero_ht; ?>">
	<div class="container">
		<div class="hero-text-outer" style="height:<?php echo $hero_ht; ?>;<?php echo the_field('header_text_width_position_hero', 'option') ?>">
			<div class="hero-text"><?php echo $bg_hero_text; ?></div>
		</div>
	</div>
</section>
<!-- END HERO HEADER -->
<?php } ?>
	
<div class="container">
    <section class="pop-podcasts">
		<h2>Most Popular</h2>
		<div class="row">
		<?php if ( have_rows( 'popular_podcasts', 'option' ) ) : ?>
		<?php while ( have_rows( 'popular_podcasts', 'option' ) ) : the_row(); ?>
		<?php $post_object = get_sub_field( 'featured_podcast' ); ?>
		<?php if ( $post_object ): ?>
		<div class="four columns">
			<?php $post = $post_object; ?>
			<?php setup_postdata( $post );  
			//$background = wp_get_attachment_image_src( get_post_thumbnail_id( $page->ID ), 'full' ); ?>
			
			<div class="pop-post-img" onclick="location.href='<?php the_permalink(); ?>'">
			<?php the_post_thumbnail('full'); ?>
				<div class="overlay"><div class="post-title remove-colon"><?php the_title(); ?></div><h1 class="listen-text">Listen Now</h1><div class="play-btn"></div></div>
			</div>	
		</div>
			<?php wp_reset_postdata(); ?>
		<?php endif; ?>
	<?php endwhile; ?>
<?php else : ?>
	<?php // no rows found ?>
<?php endif; ?>
		</div>
	</section>
		
<section class="subscribe-links">
	<div class="contianer">
		<div class="row">
  			<?php if ( have_rows( 'subscribe_buttons', 'option' ) ) : ?>
		  <div class="subscribe-buttons">
		  	<div class="subscribe-text">Subscribe</div>
				<?php while ( have_rows( 'subscribe_buttons', 'option' ) ) : the_row(); ?>
				<a href="<?php the_sub_field( 'button_link' ); ?>" class="button" target="_blank"><?php the_sub_field( 'button_label' ); ?></a>
				<?php endwhile; ?>
		  </div>
				<?php else : ?>
					<?php // no rows found ?>
				<?php endif; ?>
		</div>
	</div>
</section>
  
<div class="cat-search center">
 	<a href="#" class="search-link">Search <i class="fa fa-search" aria-hidden="true"></i></a>
  	<!--?php get_search_form(); ?--></div>
   	<?php get_template_part( 'loop', 'category' ); ?>
	<?php  get_template_part( '_template-parts/part', 'navigation' ); ?>
 </div>


 <?php  
		
	/**************************************************
	******  IF THIS IS UNDER THE WORKOUT VIDEOS  ******
	**************************************************/
	
	} elseif (in_category('workout-videos')){ ?>
	<?php //Hero Header
		
	//var
	$w_img = get_field('workout_videos_header_image', 'option');
	$w_tnt = get_field('workout_videos_header_tint', 'option');
	$w_txt = get_field('workout_videos_header_text', 'option');
		
	?>
	<!-- START HERO HEADER -->
	<section class="hero-header" style="background-image:linear-gradient(rgba(0,0,0,<?php echo $w_tnt ?>), rgba(0,0,0,<?php echo $w_tnt ?>)),url(<?php echo $w_img['url']; ?>);min-height:750px">
		<div class="container">
			<div class="hero-text-outer" style="height:750px;max-width:100%;">
				<div class="hero-text"><?php echo $w_txt ?></div>
			</div>
		</div>
	</section>
	<!-- END HERO HEADER -->

<div class="container workout-vlog-vids">
		<?php query_posts( array ( 'category_name' => 'workout-videos' ) ); ?>
    	<?php if (have_posts()) : 
			  while ( have_posts() ) : the_post(); ?>
	<div id="post-<?php the_ID(); ?>" <?php post_class('post clearfix vid-post'); ?>>
     	<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full'); ?></a>
		<a class="post-title-link" href="<?php the_permalink(); ?>"><h1 class="post-title vid-title"><?php the_title(); ?></h1></a>
		<div class="vid-post-excerpt"><?php the_excerpt(__('new_excerpt_length')); ?></div>
	</div>
		<?php endwhile;
			  endif;
			  
			  get_template_part( '_template-parts/part', 'navigation' ); 
    		  wp_reset_query();
		?>		
</div>


<?php 
	
	/**************************************************
	*********   IF THIS IS UNDER THE RECIPES  *********
	**************************************************/

	} elseif (in_category('recipes')) { ?>
	<?php //Hero Header
		
	//var
	$r_img = get_field('recipes_header_image', 'option');
	$r_tnt = get_field('recipes_header_tint', 'option');
	$r_txt = get_field('recipes_header_text', 'option');
		
	?>
	<!-- START HERO HEADER -->
	<section class="hero-header" style="background-image:linear-gradient(rgba(0,0,0,<?php echo $r_tnt ?>), rgba(0,0,0,<?php echo $r_tnt ?>)),url(<?php echo $r_img['url']; ?>);min-height:750px">
		<div class="container">
			<div class="hero-text-outer" style="height:750px;max-width:100%;">
				<div class="hero-text"><?php echo $r_txt ?></div>
			</div>
		</div>
	</section>
	<!-- END HERO HEADER -->
	
<div class="recipe-posts container">
	<div class="blog-container recipe-list">
		<div class="content-area">
		<?php while ( have_posts() ) : the_post(); ?>
       		<!-- Display All Posts -->
			<div id="post-<?php the_ID(); ?>" <?php post_class('post clearfix post-list'); ?>>
				<div class="row">
					<div class="four columns">
						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('blog-posts'); ?></a>
					</div>
					<div class="seven columns offset-by-one">
 						<div class="post-text">
  							<a class="post-title-link" href="<?php the_permalink(); ?>"><h4 class="post-title"><?php the_title(); ?></h4></a>
  							<?php the_excerpt(__('new_excerpt_length')); ?>
    						<div class="post-meta"><a href="<?php the_permalink(); ?>">READ MORE</a> <!-- &ensp;|&ensp; <!--?php comments_number(__('Comments 0')); ?--></div>	
						</div>	
					</div>
				</div>
			</div>
		<?php endwhile;
			  get_template_part( '_template-parts/part', 'navigation' ); 
			  wp_reset_query(); ?>
		</div>
	</div>
<?php get_sidebar(); ?>
	<div class="post-footer center">
		<div class="share-buttons">
		 <?php 
			$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
			$subject =  get_the_title();
			$body = get_permalink();
			$desc = get_the_excerpt();
		?>
			<span class="share-title">SHARE</span>
		   <div class="fshare"><a href="https://www.facebook.com/dialog/feed?app_id=184683071273&link=<?php the_permalink() ?>&picture=<?php echo $feat_image; ?>&name=<?php echo the_title (); ?>&caption=%20&description=<?php echo $desc; ?>&redirect_uri=http%3A%2F%2Fwww.facebook.com%2F" target="_blank"><i class="fa fa-facebook"></i></a></div>

		   <div class="tshare"><a href="http://twitter.com/home?status=Currently reading: <?php the_title ();?><?php echo get_settings('home'); ?>/?p=<?php the_ID(); ?>" target="_blank"><i class="fa fa-twitter"></i></a></div>
		</div>
	</div>
</div>

	
<?php 
											 
	/**************************************************
	*************  IF THIS IS UNDER VLOGS  ************
	**************************************************/
	
	} elseif (in_category('healthy-minds')){ ?>
	<?php //Hero Header
		
	//var
	$v_img = get_field('vlogs_header_image', 'option');
	$v_tnt = get_field('vlogs_header_tint', 'option');
	$v_txt = get_field('vlogs_header_text', 'option');
		
	?>
	<!-- START HERO HEADER -->
	<section class="hero-header" style="background-image:linear-gradient(rgba(0,0,0,<?php echo $v_tnt ?>), rgba(0,0,0,<?php echo $v_tnt ?>)),url(<?php echo $v_img['url']; ?>);min-height:750px">
		<div class="container">
			<div class="hero-text-outer" style="height:750px;max-width:100%;">
				<div class="hero-text"><?php echo $v_txt ?></div>
			</div>
		</div>
	</section>
	<!-- END HERO HEADER -->
	
<div class="container">
		<?php //query_posts( array ( 'category_name' => 'healthy-minds' ) ); 
				$args = array(
                   'cat' => '9',
                   'post_type' => 'post',
                   'posts_per_page' => 5,
                   'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1),
                   );
    			query_posts($args);
		?>
    	<?php while ( have_posts() ) : the_post(); ?>
	<div id="post-<?php the_ID(); ?>" <?php post_class('post clearfix vid-post post-list'); ?>>
    	<div class="row">
			<div class="four columns"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('vlog-posts'); ?></a></div>
			<div class="seven columns offset-by-one">
				<a class="post-title-link" href="<?php the_permalink(); ?>"><h1 class="post-title vid-title"><span class="episode-title"><?php the_title(); ?></span></h1></a>
		<div class="vid-post-excerpt"><?php the_excerpt(__('new_excerpt_length')); ?></div>
		<div class="post-meta"><a href="<?php the_permalink(); ?>">Watch Now</a></div>	
			</div>
	</div>
	</div>
		<?php endwhile; 
			  get_template_part( '_template-parts/part', 'navigation' );
			  wp_reset_query();	  	   
		?>		
</div>
		
<?php 
	
	/**************************************************
	******  IF THIS IS UNDER ANY OTHER CATEGORY ******
	**************************************************/									   
	} else { ?>
<div class="container">
	<div class="cat-search"><?php get_search_form(); ?></div>
		<?php get_template_part( 'loop', 'category' ); ?>
		<?php  get_template_part( '_template-parts/part', 'navigation' ); ?>
</div>
		<?php } ?>
	

</article>	
<?php get_footer(); ?>