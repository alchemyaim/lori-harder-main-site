<?php
/**
 * 404 Page
 *
 */

get_header();
?>
<article>

<?php //Hero Header
	//var
	$bg_hero_img = get_field('error_page_image', 'option');
	$bg_hero_text = get_field('error_page_text', 'option');	
?>

<!-- START HERO HEADER -->
<section class="hero-header" style="background-image:linear-gradient(rgba(0,0,0, 0.3), rgba(0,0,0, 0.3)),url(<?php echo $bg_hero_img['url']; ?>);height:100vh;">
	<div class="container">
		<div class="hero-text-outer" style="height:100vh;max-width:100%;">
			<div class="hero-text"><?php echo $bg_hero_text; ?></div>
		</div>
	</div>
</section>
<!-- END HERO HEADER -->


<?php get_footer(); ?>