/*  -------------------------------------------------------
	Javascript helper document insert any simple JS functions here.
------------------------------------------------------- */
jQuery(function($) {
        "use strict";
    var videos  = $(".video");

        videos.on("click", function(){
            var elm = $(this),
                conts   = elm.contents(),
                le      = conts.length,
                ifr     = null;

            for(var i = 0; i<le; i++){
              if(conts[i].nodeType == 8) ifr = conts[i].textContent;
            }

            elm.addClass("player").html(ifr);
            elm.off("click");
        });
});


jQuery(function($) {
	"use strict";
	$('.post-title').each(function () {
    $(this).html(
        $(this).html().replace(':', ':<br /><span class="episode-title">')
    );
});
	});

jQuery(function($) {
	"use strict";
	$('.single-post-title').each(function () {
    $(this).html(
        $(this).html().replace(':', ':<span class="episode-title">')
    );
});
	});

jQuery(function($) {
	"use strict";
	$('.remove-colon').each(function () {
    $(this).html(
        $(this).html().replace(':', '')
    );
});
	});

jQuery(function($) {
// Search Overlay
	"use strict";
		$('.search-link').click(function() {
			$('#search-overlay').toggle();
			$('body').toggleClass('noscroll');
			$('.wpdreams_asl_results').toggle();
			$('input.orig').focus();
		});
		$('.close-button').click(function() {
			$('#search-overlay').toggle();
			$('body').toggleClass('noscroll');
			$('.wpdreams_asl_results').toggle();
		});
	
	});

jQuery(function($) {
	"use strict";
$('#s').on('keyup', function() {
    var input = $(this);
    if(input.val().length === 0) {
        input.addClass('empty');
    } else {
        input.removeClass('empty');
    }
});
	});

/*BODY & SOUL PHOTO CAROUSEL*/
jQuery(function($) {
	"use strict";
	$('.photo-row').slick({
	  dots: false,
	  arrows: false,
	  infinite: true,
	  variableWidth: true,
	  autoplay: true,
  	  autoplaySpeed: 2000,
	  speed:2000,
	  slidesToShow: 8,
	  slidesToScroll: 2,
	  responsive: [
		{
		  breakpoint: 1024,
		  settings: {
			slidesToShow: 6,
			slidesToScroll: 2
		  }
		},
		{
		  breakpoint: 600,
		  settings: {
			slidesToShow: 4,
			slidesToScroll: 2
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
			slidesToShow: 2,
			slidesToScroll: 2
		  }
		}
		// You can unslick at a given breakpoint now by adding:
		// settings: "unslick"
		// instead of a settings object
	  ]
	});
});

jQuery(function($) {
$('#footer-mobile-menu li.menu-item-has-children>a').on('click', function(){
		$(this).removeAttr('href');
		var element = $(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp();
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown();
			element.siblings('li').children('ul').slideUp();
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp();
		}
	});

	$('#footer-mobile-menu>ul>li.menu-item-has-children>a').append('<span class="holder"></span>');

	(function getColor() {
		var r, g, b;
		var textColor = $('#footer-mobile-menu').css('color');
		textColor = textColor.slice(4);
		r = textColor.slice(0, textColor.indexOf(','));
		textColor = textColor.slice(textColor.indexOf(' ') + 1);
		g = textColor.slice(0, textColor.indexOf(','));
		textColor = textColor.slice(textColor.indexOf(' ') + 1);
		b = textColor.slice(0, textColor.indexOf(')'));
		var l = rgbToHsl(r, g, b);
		if (l > 0.7) {
			$('#footer-mobile-menu>ul>li>a').css('text-shadow', '0 1px 1px rgba(0, 0, 0, 0)');
			$('#footer-mobile-menu>ul>li>a>span').css('border-color', 'rgba(0, 0, 0, 0)');
		}
		else
		{
			$('#footer-mobile-menu>ul>li>a').css('text-shadow', '0 1px 0 rgba(255, 255, 255, 0)');
			$('#footer-mobile-menu>ul>li>a>span').css('border-color', 'rgba(255, 255, 255, 0)');
		}
	})();

	function rgbToHsl(r, g, b) {
	    r /= 255, g /= 255, b /= 255;
	    var max = Math.max(r, g, b), min = Math.min(r, g, b);
	    var h, s, l = (max + min) / 2;

	    if(max == min){
	        h = s = 0;
	    }
	    else {
	        var d = max - min;
	        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
	        switch(max){
	            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
	            case g: h = (b - r) / d + 2; break;
	            case b: h = (r - g) / d + 4; break;
	        }
	        h /= 6;
	    }
	    return l;
	}
});

/*  -------------------------------------------------------------
		DEFAULT JS FUNCTIONS BELOW THIS LINE
------------------------------------------------------------- */
/*SLIDE OUT MENU*/	
jQuery(function($) {
		// Slideout Menu
	"use strict";
    $('#slideout-trigger').on('click', function(event){
    	event.preventDefault();
    	// create menu variables
    	var slideoutMenu = $('#slideout-menu');
    	var slideoutMenuWidth = $('#slideout-menu').outerWidth();

    	// toggle open class
    	slideoutMenu.toggleClass("open");

    	// slide menu
    	if (slideoutMenu.hasClass("open")) {
	    	slideoutMenu.animate({
		    	right: "0px"
	    	});
    	} else {
	    	slideoutMenu.animate({
		    	right: -slideoutMenuWidth
	    	}, 250);
    	}
    });

		$('#nav-close').on('click', function(event){
			event.preventDefault();
			// create menu variables
    	var slideoutMenu = $('#slideout-menu');
    	var slideoutMenuWidth = $('#slideout-menu').outerWidth();

    	// toggle open class
    	slideoutMenu.toggleClass("open");

    	// slide menu
    	if (slideoutMenu.hasClass("open")) {
	    	slideoutMenu.animate({
		    	right: "0px"
	    	});
    	} else {
	    	slideoutMenu.animate({
		    	right: -slideoutMenuWidth
	    	}, 250);
    	}
    });
	
	/*SLIDE OUT MENU DROPDOWN*/	
    $('#slideout-menu ul ul').hide();
    if ($('#slideout-menu .menu-item-has-children').length > 0) {
        $('#slideout-menu .menu-item-has-children').click(

        function () {
            $(this).addClass('toggled');
            if ($(this).hasClass('toggled')) {
                $(this).children('ul').slideToggle();
            }
            //return false;

        });
    }
});
  
//Scrolling Anchor Link
jQuery(function($) {
	"use strict";
		$('a[href*=#]:not([href=#])').click(function() {
		
    if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') || location.hostname === this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
           if (target.length) {
             $('html,body').animate({
                 scrollTop: target.offset().top
            }, 1000);
            return false;
        }
    }
});
});