<?php

$current_theme = wp_get_theme();

/* ----------------------
----- Button Config -----
-------------------------*/

if ( ! isset( $aa_shortcodes['button'] ) ) {
$aa_shortcodes['button'] = array(
	'title' => __('Button', 'textdomain'),
	'shortcode' => '[button style="{{style}}" link="{{link}}" target="{{target}}" label="{{label}}" ][/button]',
	'popup_title' => __('Insert Button', 'textdomain'),
	'params' => array(
		'style' => array(
			'type' => 'select',
			'label' => __('Style:', 'textdomain'),
			'desc' => __('Select the button\'s style, ie the button\'s colour', 'textdomain'),
			'options' => array(
				'button-primary' => 'Button',
				'' => 'Ghost Button'
			)
		),
		'link' => array(
			'std' => '#',
			'type' => 'text',
			'label' => __('Link:', 'textdomain'),
			'desc' => __('Add the button\'s url eg http://example.com', 'textdomain')
		),
		'target' => array(
			'type' => 'select',
			'label' => __('Target:', 'textdomain'),
			'desc' => __('Set the browser behavior for the click action.', 'textdomain'),
			'options' => array(
				'_self' => 'Same window',
				'_blank' => 'New window'
			)
		),
		'label' => array(
			'std' => 'Button',
			'type' => 'text',
			'label' => __('Label:', 'textdomain'),
			'desc' => __('Add the button\'s text', 'textdomain'),
		),
	)	
);
}

/* ----------------------
----- Fonts Config -----
-------------------------*/

if ( ! isset( $aa_shortcodes['font'] ) ) {
$aa_shortcodes['font'] = array(
	'title' => __('Fonts', 'textdomain'),
	'shortcode' => '[font style="{{style}}"]{{text}}[/font]',
	'popup_title' => __('Font Style', 'textdomain'),
	'params' => array(
		'style' => array(
			'type' => 'select',
			'label' => __('Font:', 'textdomain'),
			'desc' => __('Select the font style to wrap you text with.', 'textdomain'),
			'options' => array(
				'script-font' => 'Script Heading',
				'' => 'Standard Heading'
			)
		),
		'text' => array(
			'std' => '',
			'type' => 'textarea',
			'label' => __('Text:', 'textdomain'),
			'desc' => __('Enter the text here.', 'textdomain'),
		),
	)	
);
}

/* ----------------------
----- Column Config -----
-------------------------*/

if ( ! isset( $aa_shortcodes['columns'] ) ) {

	$aa_shortcodes['columns'] = array(
		'params' => array(),
		'shortcode' => ' {{child_shortcode}} ', // as there is no wrapper shortcode
		'popup_title' => __('Insert Columns', 'textdomain'),
		'no_preview' => true,
		
		// child shortcode is clonable & sortable
		'child_shortcode' => array(
			'params' => array(
				'column' => array(
					'type' => 'select',
					'label' => __('Column Type', 'textdomain'),
					'desc' => __('Select the type, ie width of the column.', 'textdomain'),
					'options' => array(
						'full_width' => 'Full Width (1/1)',
						'one_half' => 'One Half (1/2)',
						'one_third' => 'One Third (1/3)',
						'one_fourth' => 'One Fourth (1/4)',
						'two_third' => 'Two Thirds (2/3)',
						'three_fourth' => 'Three Fourths (3/4)',
						'one_fifth' => 'One Fifth (1/5)',
						'two_fifth' => 'Two Fifth (2/5)',
						'three_fifth' => 'Three Fifth (3/5)',
						'one_sixth' => 'One Sixth (1/6)'
					)
				),
				'el_last' => array(
					'type' => 'select',
					'std' => '',
					'label' => __('Last Column', 'textdomain'),
					'desc' => __('Check this if the current column is the last one in a row.', 'textdomain'),
					'options' => array(
						'' => 'No',
						'last' => 'Yes'
					)
				),
				'content' => array(
					'std' => '',
					'type' => 'textarea',
					'label' => __('Column Content', 'textdomain'),
					'desc' => __('Add the column content.', 'textdomain'),
				)
			),
			'shortcode' => '[{{column}} {{el_last}}] {{content}} [/{{column}}] ',
			'clone_button' => __('Add Column', 'textdomain')
		)
	);

}
/* -------------------------
----- Clear Fix Config -----
----------------------------*/

if ( ! isset( $aa_shortcodes['clearfix'] ) ) {
$aa_shortcodes['clearfix'] = array(
	'title' => __('Clear Fix', 'textdomain'),
	'shortcode' => '[clear][/clear] ',
	'popup_title' => __('Insert Clear Fix', 'textdomain'),
	'params' => array(
		'style' => array(
			'type' => 'text',
			'label' => __('Clear Fix:', 'textdomain'),
			'std' => 'There are no options for this shortcode.',
		)
	)
);
}


/* ---------------------
----- Alert Config -----
------------------------*/

if ( ! isset( $aa_shortcodes['alert'] ) ) {
$aa_shortcodes['alert'] = array(
	'title' => __('Alert box', 'textdomain'),
	'shortcode' => '[alert_box style="{{style}}" close="{{close}}"] {{content}} [/alert_box]',
	'popup_title' => __('Insert An Alert', 'textdomain'),
	'params' => array(
		'style' => array(
			'type' => 'select',
			'label' => __('Type:', 'textdomain'),
			'desc' => __('Select the alert type.', 'textdomain'),
			'options' => array(
				'info' => 'Info',
				'note' => 'Note',
				'download' => 'Download',
				'warning' => 'Warning'
			)
		),
		'close' => array(
					'type' => 'select',
					'std' => '',
					'label' => __('Closable', 'textdomain'),
					'desc' => __('Check this if you want this alert box to be closeable.', 'textdomain'),
					'options' => array(
						'no' => 'No',
						'yes' => 'Yes'
					)
				),
		'content' => array(
			'std' => '',
			'type' => 'textarea',
			'label' => __('Content:', 'textdomain'),
			'desc' => __('Add the alert\'s text', 'textdomain'),
		)
	)
);
}


/* --------------------
----- Tabs Config -----
-----------------------*/

if ( ! isset( $aa_shortcodes['tabs'] ) ) {

	$aa_shortcodes['tabs'] = array(
	    'params' => array(
	    	'style' => array(
	            'std' => 'centered',
	            'type' => 'select',
	            'label' => __('Style:', 'textdomain'),
	            'desc' => __('Choose whether you would like your tabs centered or left justified.', 'textdomain'),
				'options' => array(
					'centered' => 'Centered',
					'left' => 'Left'
				)
			),
	    ),
	    'no_preview' => true,
	    'shortcode' => '[tabs style="{{style}}"] {{child_shortcode}} [/tabs]',
	    'popup_title' => __('Insert Tabs', 'textdomain'),
	    'child_shortcode' => array(
	        'params' => array(
	            'title' => array(
	                'std' => '',
	                'type' => 'text',
	                'label' => __('Title:', 'textdomain'),
	                'desc' => __('Title of the tab.', 'textdomain'),
	            ),
	            'content' => array(
	                'std' => '',
	                'type' => 'textarea',
	                'label' => __('Tab Content:', 'textdomain'),
	                'desc' => __('Add the tabs content here.', 'textdomain')
	            )
	        ),
	        'shortcode' => '[tab title="{{title}}"] {{content}} [/tab]',
	        'clone_button' => __('Add Tab', 'textdomain')
	    )
	);

}

/* ----------------------
----- Toggle Config -----
-------------------------*/

if ( ! isset( $aa_shortcodes['toggle'] ) ) {
$aa_shortcodes['toggle'] = array(
	'title' => __('Toggles', 'textdomain'),
	'shortcode' => ' {{child_shortcode}} ', // There is no wrapper shortcode
	'notes' => __('Click \'Add Toggle\' to add a new toggle. Drag and drop to reorder toggles.', 'textdomain'),
	'params' => array(),
	'child_shortcode' => array(
		'params' => array(
			'title' => array(
				'type' => 'text',
				'label' => __('Toggle Content Title', 'textdomain'),
				'desc' => __('Add the title that will go above the toggle content', 'textdomain'),
				'std' => 'Title'
			),
			'content' => array(
				'std' => 'Content',
				'type' => 'textarea',
				'label' => __('Toggle Content', 'textdomain'),
				'desc' => __('Add the toggle content. Will accept HTML', 'textdomain'),
			)
		),
		'shortcode' => '[toggle  title="{{title}}"] {{content}} [/toggle]',
		'popup_title' => __('Insert Toggles', 'textdomain'),
		'clone_button' => __('Add Toggle', 'textdomain')
	)
);
}

/* ---------------------------
-----   Accordion Config -----
------------------------------*/

if ( ! isset( $aa_shortcodes['accordion'] ) ) {

	$aa_shortcodes['accordion'] = array(
	    'params' => array(
	    	'opened' => array(
	            'std' => '0',
	            'type' => 'text',
	            'label' => __('Opened', 'textdomain'),
	            'desc' => __('You can choose which of the sections will be opened at load. "0" is the first, while "-1" will leave all sections closed.', 'textdomain')
			)
	    ),
	    'no_preview' => true,
	    'shortcode' => '[accordion] {{child_shortcode}} [/accordion]',
	    'popup_title' => __('Insert Accordion', 'textdomain'),
	    'child_shortcode' => array(
	        'params' => array(
	            'title' => array(
	                'std' => 'Title',
	                'type' => 'text',
	                'label' => __('Section Title', 'textdomain'),
	                'desc' => __('Title of the accordion section.', 'textdomain'),
	            ),
	            'content' => array(
	                'std' => '',
	                'type' => 'textarea',
	                'label' => __('Section Content', 'textdomain'),
	                'desc' => __('Add the accordion section content.', 'textdomain')
	            )
	        ),
	        'shortcode' => '[accordion_section title="{{title}}"] {{content}} [/accordion_section]',
	        'clone_button' => __('Add Section', 'textdomain')
	    )
	);

}

/* ------------------------------
-----  Google Map Config -------
---------------------------------*/

if ( ! isset( $aa_shortcodes['google-map'] ) ) {
$aa_shortcodes['google-map'] = array(
	'title' => __('Google Map', 'textdomain'),
	'shortcode' => '[vsgmap address="{{address}}"]',
	'popup_title' => __('Insert A Google Map', 'textdomain'),
	'params' => array(
		'address' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Address:', 'textdomain'),
			'desc' => __('Add an address eg 123 Fifth Avenue, New York, NY 10003.', 'textdomain')
		),
	)
);
}

/* ----------------------
----- Video Config -----
-------------------------*/
if ( ! isset( $aa_shortcodes['video'] ) ) {
$aa_shortcodes['video'] = array(
	'title' => __('Video Embed', 'textdomain'),
	'shortcode' => '[embed-video source="{{source}}" id={{id}}]',
	'popup_title' => __('Embed A Video', 'textdomain'),
	'params' => array(
		'source' => array(
			'type' => 'select',
			'label' => __('Video Source:', 'textdomain'),
			'desc' => __('Select the video source, either YouTube or Vimeo', 'textdomain'),
			'options' => array(
				'vimeo' => 'Vimeo',
				'youtube' => 'YouTube'
			)
		),
		'id' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Video ID:', 'textdomain'),
			'desc' => __('Enter the video ID.', 'textdomain'),
		),
	)	
);
}


/* ----------------------------
----- Social Icons Config -----
-------------------------------*/

if ( ! isset( $aa_shortcodes['social'] ) ) {

	$aa_shortcodes['social'] = array(
	    'no_preview' => true,
	    'shortcode' => '[social]{{child_shortcode}}[/social]',
	    'popup_title' => __('Insert Social Icons', 'textdomain'),
	    'child_shortcode' => array(
	        'params' => array(
	            'type' => array(
	                'type' => 'select',
	                'label' => __('Icon Type', 'textdomain'),
	                'desc' => __('Choose the icon\'s type (social network).', 'textdomain'),
					'options' => array(
						'fa-behance' => 'Behance',
						'fa-envelope-o' => 'Email',
						'fa-facebook' => 'Facebook',
						'fa-flickr' => 'Flickr',
						'fa-google-plus' => 'Google Plus',
						'fa-instagram' => 'Instagram',
						'fa-linkedin' => 'LinkedIn',
						'fa-pinterest-p' => 'Pinterest',
						'fa-rss' => 'RSS',
						'fa-twitter' => 'Twitter',
						'fa-vimeo' => 'Vimeo',
						'fa-youtube' => 'YouTube'
					)
	            ),
				
			'target' => array(
				'type' => 'select',
				'label' => __('Icon Target', 'textdomain'),
				'desc' => __('_self = open in same window. _blank = open in new window.', 'textdomain'),
				'options' => array(
					'_self' => '_self',
					'_blank' => '_blank'
				
			)
		),
	            'url' => array(
	            	'std' => '',
	                'type' => 'text',
	                'label' => __('Icon Url', 'textdomain'),
	                'desc' => __('Add the url to your social profile related with the chosen icon.', 'textdomain')
	            )
	        ),
			
	        'shortcode' => '[link type="{{type}}" href="{{url}}" target="{{target}}"/]',
	        'clone_button' => __('Add Icon', 'textdomain')
	    )
	);

}


?>