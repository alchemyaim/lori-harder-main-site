
// start the popup specefic scripts
// safe to use $
jQuery(document).ready(function($) {
    var aa = {
    	loadVals: function()
    	{
    		var shortcode = $('#_aa_shortcode').text(),
    			uShortcode = shortcode;
    		
    		// fill in the gaps eg {{param}}
    		$('.aa-input').each(function() {
    			var input = $(this),
    				id = input.attr('id'),
    				id = id.replace('aa_', ''),		// gets rid of the aa_ prefix
    				re = new RegExp("{{"+id+"}}","g");
    				
    			uShortcode = uShortcode.replace(re, input.val());
    		});
    		
    		// adds the filled-in shortcode as hidden input
    		$('#_aa_ushortcode').remove();
    		$('#aa-sc-form-table').prepend('<div id="_aa_ushortcode" class="hidden">' + uShortcode + '</div>');
    	},
    	cLoadVals: function()
    	{
    		var shortcode = $('#_aa_cshortcode').text(),
    			pShortcode = '';
    			shortcodes = '';
    		
    		// fill in the gaps eg {{param}}
    		$('.child-clone-row').each(function() {
    			var row = $(this),
    				rShortcode = shortcode;
    			
    			$('.aa-cinput', this).each(function() {
    				var input = $(this),
    					id = input.attr('id'),
    					id = id.replace('aa_', '')		// gets rid of the aa_ prefix
    					re = new RegExp("{{"+id+"}}","g");
    					
    				rShortcode = rShortcode.replace(re, input.val());
    			});
    	
    			shortcodes = shortcodes + rShortcode + "\n";
    		});
    		
    		// adds the filled-in shortcode as hidden input
    		$('#_aa_cshortcodes').remove();
    		$('.child-clone-rows').prepend('<div id="_aa_cshortcodes" class="hidden">' + shortcodes + '</div>');
    		
    		// add to parent shortcode
    		this.loadVals();
    		pShortcode = $('#_aa_ushortcode').text().replace('{{child_shortcode}}', shortcodes);
    		
    		// add updated parent shortcode
    		$('#_aa_ushortcode').remove();
    		$('#aa-sc-form-table').prepend('<div id="_aa_ushortcode" class="hidden">' + pShortcode + '</div>');
    	},
    	children: function()
    	{
    		// assign the cloning plugin
    		$('.child-clone-rows').appendo({
    			subSelect: '> div.child-clone-row:last-child',
    			allowDelete: false,
    			focusFirst: false
    		});
    		
    		// remove button
    		$('.child-clone-row-remove').live('click', function() {
    			var	btn = $(this),
    				row = btn.parent();
    			
    			if( $('.child-clone-row').size() > 1 )
    			{
    				row.remove();
    			}
    			else
    			{
    				alert('You need a minimum of one row');
    			}
    			
    			return false;
    		});
    		
    		// assign jUI sortable
    		$( ".child-clone-rows" ).sortable({
				placeholder: "sortable-placeholder",
				items: '.child-clone-row'
				
			});
    	},
    	resizeTB: function()
    	{
			var	ajaxCont = $('#TB_ajaxContent'),
				tbWindow = $('#TB_window'),
				aaPopup = $('#aa-popup');

            tbWindow.css({
                height: aaPopup.outerHeight() + 50,
                width: aaPopup.outerWidth(),
                marginLeft: -(aaPopup.outerWidth()/2)
            });

			ajaxCont.css({
				paddingTop: 0,
				paddingLeft: 0,
				paddingRight: 0,
				height: (tbWindow.outerHeight()-47),
				overflow: 'auto', // IMPORTANT
				width: aaPopup.outerWidth()
			});
			
			$('#aa-popup').addClass('no_preview');
    	},
    	load: function()
    	{
    		var	aa = this,
    			popup = $('#aa-popup'),
    			form = $('#aa-sc-form', popup),
    			shortcode = $('#_aa_shortcode', form).text(),
    			popupType = $('#_aa_popup', form).text(),
    			uShortcode = '';
    		
    		// resize TB
    		aa.resizeTB();
    		$(window).resize(function() { aa.resizeTB() });
    		
    		// initialise
    		aa.loadVals();
    		aa.children();
    		aa.cLoadVals();
    		
    		// update on children value change
    		$('.aa-cinput', form).live('change', function() {
    			aa.cLoadVals();
    		});
    		
    		// update on value change
    		$('.aa-input', form).change(function() {
    			aa.loadVals();
    		});
    		
    		// when insert is clicked
    		$('.aa-insert', form).click(function() {    		 
    			if(window.tinyMCE)
				{ 
                    window.tinyMCE.execCommand('mceInsertContent', false, $('#_aa_ushortcode', form).html());
					tb_remove();
				}
    		});
    	}
	}
    
    // run
    $('#aa-popup').livequery( function() { aa.load(); } );
});