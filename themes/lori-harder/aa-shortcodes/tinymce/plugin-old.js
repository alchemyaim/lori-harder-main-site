(function ()
{
	// create aaShortcodes plugin
	tinymce.create("tinymce.plugins.aaShortcodes",
	{
		init: function ( ed, url )
		{
			ed.addCommand("aaPopup", function ( a, params )
			{
				var popup = params.identifier;
				
				// load thickbox
				tb_show("Shortcodes", url + "/popup.php?popup=" + popup + "&width=700");

				jQuery('#custom-tb').remove();

				jQuery('head').append('<style id="custom-tb">#aa-popup { height: ' + (document.documentElement.clientHeight-120) + 'px !important; } #TB_ajaxContent { background: #fff; }</style>');

			});
		},
		createControl: function ( btn, e )
		{
			if ( btn == "aa_button" )
			{	
				var a = this;

				var btn = e.createSplitButton('aa_button', {
                    title: "Shortcodes",
					image: aaShortcodes.pluginFolder +"/tinymce/images/icon.png",
					icons: false
                });

                btn.onRenderMenu.add(function (c, b)
				{					

					for(var i = 0; i < aaShortcodes.shortcodesList.length; i++){
						a.addWithPopup( b, aaShortcodes.shortcodesList[i][0], aaShortcodes.shortcodesList[i][1] );
					}

				});
                
                return btn;
			}
			
			return null;
		},
		addWithPopup: function ( ed, title, id ) {
			ed.add({
				title: title,
				onclick: function () {
					tinyMCE.activeEditor.execCommand("aaPopup", false, {
						title: title,
						identifier: id
					})
				}
			})
		},
		addImmediate: function ( ed, title, sc) {
			ed.add({
				title: title,
				onclick: function () {
					tinyMCE.activeEditor.execCommand( "mceInsertContent", false, sc )
				}
			})
		},
		getInfo: function () {
			return {
				longname: 'Shortcodes',
				author: 'aa. graphics & web design, inc.',
				authorurl: 'http://aadesigns.com',
				infourl: 'http://wiki.moxiecode.com/',
				version: "1.0"
			}
		}
	});
	
	// add aaShortcodes plugin
	tinymce.PluginManager.add("aaShortcodes", tinymce.plugins.aaShortcodes);

})();