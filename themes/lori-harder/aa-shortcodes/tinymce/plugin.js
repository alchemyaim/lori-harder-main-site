(function() {
"use strict";  
 
    tinymce.PluginManager.add( 'aaShortcodes', function( editor, url ) {

    	editor.addCommand("aaPopup", function ( a, params ) {

			var popup = params.identifier;
		
			// load thickbox
			tb_show("Alchemy + Aim Shortcodes", url + "/popup.php?popup=" + popup + "&width=700");

			jQuery('#custom-tb').remove();

			jQuery('head').append('<style id="custom-tb">#aa-popup { height: ' + (document.documentElement.clientHeight-120) + 'px !important; } #TB_ajaxContent { background: #fff; }</style>');

		});

		// Create values array

		var valuesArr = Array();

		for(var i = 0; i < aaShortcodes.shortcodesList.length; i++) {

			valuesArr[i] = {
				text: aaShortcodes.shortcodesList[i][0],
				id: aaShortcodes.shortcodesList[i][1],
				onClick: function(){
					tb_show("Alchemy + Aim Shortcodes", url + "/popup.php?popup=" + this._id + "&width=700");
					jQuery('#custom-tb').remove();
					jQuery('head').append('<style id="custom-tb">#aa-popup { height: ' + (document.documentElement.clientHeight-120) + 'px !important; } #TB_ajaxContent { background: #fff; }</style>');
				}
			};

		}

        editor.addButton( 'aa_button', {
            type: 'listbox',
            text: 'Insert Shortcode',
            icon: 'ks',
            onselect: function(e) {},
            values: valuesArr
        });
 
    });
 
})();