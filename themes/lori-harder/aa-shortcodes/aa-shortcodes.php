<?php

/*
Plugin Name: Alchemy + Aim Shortcodes
Plugin URI: http://www.alchemyandaim.com
Description: A shortcode generator to add buttons, columns, tabs, toggles and more to your theme.
Version: 2.0
Author: Alchemy + Aim | Phillip De Vita
Author URI: http://www.alchemyandaim.com
*/


class Shortcodes {

    function __construct() {	
	
    	$current_theme = wp_get_theme();
		
		/** Create a filter for "Theme Mode" **/
		define( 'AASC_THEME_MODE', apply_filters( 'aasc_theme_mode', false ) );

    	require_once( 'shortcodes.php' );
		
		if ( false == AASC_THEME_MODE) {
			define( 'TINYMCE_URI', plugin_dir_url( __FILE__ ) .'tinymce');
			define( 'TINYMCE_DIR', plugin_dir_url( __FILE__ ) .'tinymce');
			define( 'PLUGIN_URI', plugin_dir_url( __FILE__ ));
			define( 'PLUGIN_DIR', plugin_dir_url( __FILE__ ));
      } else {
        if ( true == AASC_THEME_MODE ) {
			  $path = ltrim( end( @explode( get_stylesheet(), str_replace( '\\', '/', dirname( __FILE__ ) ) ) ), '/' );
			  define( 'TINYMCE_URI', get_bloginfo('template_directory') . '/' . $path . '/tinymce');
			  define( 'TINYMCE_DIR', get_bloginfo('template_directory') . '/' . $path . '/tinymce');
			  define( 'PLUGIN_URI',  trailingslashit(trailingslashit(get_bloginfo('template_directory') ). $path));
			  define( 'PLUGIN_DIR', trailingslashit(trailingslashit(get_bloginfo('template_directory') ) . $path ));
      	}
	  }

        add_action( 'init', array( &$this, 'init' ) );
        add_action( 'admin_init', array( &$this, 'admin_init' ) );

}
	
	/** Registers TinyMCE rich editor buttons **/
	function init() {

    	$current_theme = wp_get_theme();


			wp_enqueue_style( 'fontawesome', PLUGIN_URI  . 'assets/font-awesome/css/font-awesome.min.css' );
			wp_enqueue_style( 'shortcodes-style', PLUGIN_URI  . 'assets/shortcodes.css' );
			wp_enqueue_script( 'shortcodes-script', PLUGIN_URI  . 'assets/shortcodes.js', array( 'jquery' ) );

		
		if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
			return;
		}
	
		if ( get_user_option( 'rich_editing' ) == 'true' ) {
			add_filter( 'mce_external_plugins', array( &$this, 'add_rich_plugins' ) );
			add_filter( 'mce_buttons', array( &$this, 'register_rich_buttons' ) );

		}

	}
	
	// --------------------------------------------------------------------------
	
	/** Defines TinyMCE rich editor js plugin **/
	function add_rich_plugins( $plugin_array ) {
		global $wp_version;

		if ( version_compare( $wp_version, '3.9', '<' ) ) {
			$plugin_array['aaShortcodes'] = TINYMCE_URI . '/plugin-old.js';
		} else {
			$plugin_array['aaShortcodes'] = TINYMCE_URI . '/plugin.js';
		}
		return $plugin_array;
	}

	function ubl_add_tinymce_button( $buttons ) {
	 
	    array_push( $buttons, shortcodes );
	    return $buttons;
	     
	}

	// --------------------------------------------------------------------------
	
	/** Adds TinyMCE rich editor buttons **/
	function register_rich_buttons( $buttons ) {
		array_push( $buttons, "|", 'aa_button' );
		return $buttons;
	}
	
	/** Enqueue Scripts and Styles **/
	function admin_init() {

		global $wp_version;

		// css
		wp_enqueue_style( 'aa-popup', TINYMCE_URI . '/css/popup.css', false, '1.0', 'all' );
		
		// js
		wp_enqueue_script( 'jquery-ui-sortable' );
		wp_enqueue_script( 'jquery-livequery', TINYMCE_URI . '/js/jquery.livequery.js', false, '1.1.1', false );
		wp_enqueue_script( 'jquery-appendo', TINYMCE_URI . '/js/jquery.appendo.js', false, '1.0', false );
		wp_enqueue_script( 'base64', TINYMCE_URI . '/js/base64.js', false, '1.0', false );
		wp_enqueue_script( 'aa-popup', TINYMCE_URI . '/js/popup.js', false, '1.0', false );
		
		// vars
    	$current_theme = wp_get_theme();
    	
		$shortcodesList = array(
			
			array( 'Button', 'button' ),
			array( 'Font', 'font' ),
			array( 'Columns', 'columns' ),
			array( 'Clear Fix', 'clearfix'),
			/*array( 'Alert', 'alert' ),
			array( 'Tabs', 'tabs' ),
			array( 'Toggles', 'toggle' ),
			array( 'Accordion', 'accordion' ),
			array( 'Google Map', 'google-map' ),
			array( 'Video', 'video' ),
			array( 'Social Icons', 'social' )*/
		
		);

		wp_localize_script( 
			'jquery', 
			'aaShortcodes', 
			array( 
				'pluginFolder' => PLUGIN_URL . '/shortcodes/',
				'shortcodesList' => $shortcodesList
			) 
		);

	}
    
}

$aa_shortcodes = new Shortcodes();
 


?>