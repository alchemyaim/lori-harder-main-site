<?php get_header(); ?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div id="header-image" class="default <?php the_field('header_color'); ?> <?php the_field('header_position'); ?> <?php echo get_the_title(); ?>">
	<img src="<?php the_field('header_image'); ?>">
	<div class="inner"><div class="text"><?php the_field('header_title'); ?><span><?php the_field('header_subtitle'); ?></span></div></div>
	<div style="clear:both;"></div>
</div>
<?php include (TEMPLATEPATH . '/newsletter_optin.php'); ?>


<div id="main-wrapper">

	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<h1><?php the_field('page_title'); ?></h1>
		<div class="page-body">
			<?php the_content('READ MORE'); ?>
		</div>	
         
	</div>
				
					
		</div>	
</div>
<?php endwhile; endif; ?>	

     <div style="clear: both;"></div>
</div></div>
<?php get_footer(); ?>