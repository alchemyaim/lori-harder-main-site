<?php if (is_single()) : ?>

<div class="navigation">
	<span class="blog-pager-older-link"><?php previous_post_link('%link', 'Older') ?></span>
	<span class="blog-pager-newer-link"><?php next_post_link('%link', 'Newer') ?></span>
</div>
<div style="clear:both;"></div>

<?php else : ?>

<div class="navigation">
	<div class="blog-pager-older-link"><?php next_posts_link('Older') ?></div>
	<div class="blog-pager-newer-link"><?php previous_posts_link('Newer') ?></div>
</div>
<div style="clear:both;"></div>

<?php endif; ?>