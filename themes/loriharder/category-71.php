<?php get_header(); ?>

<div id="header-image" class="freeworkouts">
	<img src="<?php the_field('header_image', 'options'); ?>">
	<div class="inner"><div class="text"><?php the_field('header_title', 'options'); ?><span><?php the_field('header_subtitle', 'options'); ?></span></div></div>
	<div style="clear:both;"></div>
</div>
<?php include (TEMPLATEPATH . '/newsletter_optin.php'); ?>

<div id="outer-wrapper"><div id="freeworkouts" class="single">
	<div id="main-wrapper">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
               <h1><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></h1>

		<div class="post-body">
			<?php the_content('...'); ?>
			<div style="text-align:center;"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full'); ?></a></div>
		</div>	

		<div class="social-share">
			<span>Share This!</span>
			<div class="fshare"><a href="http://www.facebook.com/sharer.php?u=<?php the_permalink() ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/share_facebook.png"  /></a></div>
			<div class="tshare"><a href="http://twitter.com/home?status=Currently reading on LoriHarder.com: <?php the_title ();?> <?php echo get_settings('home'); ?>/?p=<?php the_ID(); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/share_twitter.png"  /></a></div>
			<div class="pshare"><?php $pinterestimage = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?><a href="http://pinterest.com/pin/create/button/?url=<? the_permalink(); ?>&media=<?php echo $pinterestimage[0]; ?>&description=<?php the_title();?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/share_pinterest.png"  /></a></div>
		</div>
	</div>
        	<div style="clear: both;"></div>

	<?php endwhile; ?>
	<div class="navigation">
		<div class="blog-pager-older-link"><?php next_posts_link('Older Podcasts') ?></div>
		<div class="blog-pager-newer-link"><?php previous_posts_link('Newer Podcasts') ?></div>
	</div>			
<?php endif; ?>	
</div>
<div id="sidebar-wrapper">
	<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Podcasts Sidebar')) : ?><?php endif; ?>
</div>
</div></div>

<?php get_footer(); ?>	