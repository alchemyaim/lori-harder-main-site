<?php
/*
Template Name: Event
*/
?>
<?php get_header(); ?>

<div id="header-image" class="event">
	<img src="<?php the_field('header_image'); ?>">
	<div class="inner"><div class="text"><?php the_field('header_title'); ?><span><?php the_field('header_subtitle'); ?></span></div></div>
	<div style="clear:both;"></div>
</div>
<?php include (TEMPLATEPATH . '/newsletter_optin.php'); ?>


<div id="main-wrapper" class="event">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<h1><?php the_field('page_title'); ?></h1>
		<div class="left">
			<?php the_content(); ?>
			<div class="details"><?php the_field('event_details'); ?></div>
			<div class="buynow"><a href="<?php the_field('button_link'); ?>" target="_blank"><?php the_field('button_text'); ?></a></div>
		</div>	
		<div class="right">
			<?php the_field('right_images'); ?>
		</div>	
		<div style="clear: both;"></div>
	
		<div class="includes">
			<div class="top"><?php the_field('box_title'); ?></div>
			<div class="text"><?php the_field('box_content'); ?></div>
		</div>
		<div class="purchase">
			<div class="buynow"><a href="<?php the_field('button_link'); ?>" target="_blank"><?php the_field('button_text'); ?></a></div>
			<div class="details"><?php the_field('event_details'); ?></div>
		</div>
         
	</div>
	<?php endwhile; endif; ?>				
					
		</div>	
</div>

     <div style="clear: both;"></div>
</div></div>
<?php get_footer(); ?>