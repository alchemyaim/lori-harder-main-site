<?php
/*
Template Name: Online Coaching
*/
?>
<?php get_header(); ?>

<div id="header-image" class="onlinecoaching">
	<img src="<?php the_field('header_image'); ?>">
	<div class="inner"><div class="text"><?php the_field('header_title'); ?><span><?php the_field('header_subtitle'); ?></span></div></div>
	<div style="clear:both;"></div>
</div>
<?php include (TEMPLATEPATH . '/newsletter_optin.php'); ?>


<div id="main-wrapper" class="onlinecoaching">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="video"><?php the_field('video_area'); ?></div>
		<div class="page-body">
			<?php the_content(); ?>
		</div>	
	
		<div class="includes">
			<div class="top"><?php the_field('included_title'); ?><span><?php the_field('included_subtitle'); ?></span></div>
			<div class="text"><?php the_field('included_details'); ?></div>
		</div>
		<div class="purchasebutton">
			<a href="<?php the_field('button_link') ?>"><?php the_field('button_text'); ?></a>
		</div>
         
	</div>
	<?php endwhile; endif; ?>				
					
		</div>	
</div>

     <div style="clear: both;"></div>
</div></div>
<?php get_footer(); ?>