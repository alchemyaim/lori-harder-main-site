<?php
/*
Template Name: Meditations
*/
?>
<?php get_header(); ?>

<div id="header-image" class="meditations">
	<img src="<?php the_field('header_image'); ?>">
	<div class="inner"><div class="text"><?php the_field('header_title'); ?><span><?php the_field('header_subtitle'); ?></span></div></div>
	<div style="clear:both;"></div>
</div>
<div id="header-optin"><div class="inner">
<div class="text">Magnetize your life! Sign up to receive three FREE meditations, along with weekly goodies to unleash your inner POWERHOUSE</div>
<script type="text/javascript" src="//forms.ontraport.com/v2.4/include/formEditor/genjs-v3.php?html=false&uid=p2c26730f11"></script><div class="moonray-form-p2c26730f11 ussr"><div class="moonray-form moonray-form-label-pos-stacked">
<form class="moonray-form-clearfix" action="https://forms.ontraport.com/v2.4/form_processor.php?" method="post" accept-charset="UTF-8">
<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-text"><input name="firstname" type="text" class="moonray-form-input" id="mr-field-element-478418649478" placeholder="NAME"/></div>
<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-email"><input name="email" type="email" class="moonray-form-input" id="mr-field-element-670654755192" placeholder="EMAIL"/></div>
<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-submit"><input type="submit" name="submit-button" value="YES PLEASE!" class="moonray-form-input" id="mr-field-element-195998811846" src/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="afft_" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="aff_" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="sess_" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="ref_" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="own_" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="oprid" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="contact_id" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_source" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_medium" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_term" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_content" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_campaign" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="referral_page" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="uid" type="hidden" value="p2c26730f11"/></div>
</form></div></div>
</div></div>


<div id="main-wrapper" class="meditations">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="page-body">
			<?php the_content(); ?>
		</div>

		<div style="clear: both;"></div>
         
	</div>
	<?php endwhile; endif; ?>				
						
</div>

<div id="med-benefits"><div class="inner">
	<h3><?php the_field('benefits_title'); ?></h3>
	<h3 class="bottom"><?php the_field('benefits_subtitle'); ?></h3>
	<div class="text"><?php the_field('benefits_list'); ?></div>
</div></div>

	<div id="main-wrapper">
		<ul class="meddownloads">
		<?php if ( get_field('meditations') ) : while ( has_sub_field('meditations') ) : ?>
			<li class="meditation">
			<div class="image"><img src="<?php the_sub_field('image'); ?>"></div>
			<div class="headline"><?php the_sub_field('headline'); ?></div>
			<div class="description"><?php the_sub_field('description'); ?></div>
			<a href="<?php the_sub_field('button_link'); ?>" class="buy"><?php the_sub_field('button_text'); ?></a>
			</li>
		<?php endwhile; endif; ?>
		</ul>
	</div>

     <div style="clear: both;"></div>
</div></div>
<?php get_footer(); ?>