<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width" /> 	
	<title><?php wp_title('',true); ?></title>	
	<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" /> <!-- leave this for stats -->	
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
	<link href='http://fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/responsive.css" type="text/css" media="screen" />
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<link rel="icon" type="image/x-icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico" />
	<link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/images/favicon.png" />
	<?php if ( is_singular() ) { wp_enqueue_script( 'comment-reply' ); } ?>
	<script src="//use.typekit.net/wvd4sqy.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>
	<?php wp_head(); ?>
<meta name="google-site-verification" content="2Lwt_M3U3hJzNQolx24AyCJQWeFpWFd7HVegYqOz9FA" />
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '944910772274376');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=944910772274376&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code --></head>
<body>
<div id="outer-wrapper">
<div id="header-wrapper"><div class="inner">
	<div id="header" class="header">
		<a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/images/logo.png"  /></a>
    	</div> 
        <div id="linkbar" class="linkbar section">
		<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' =>   'primary' ) ); ?>
	</div>   
	<div style="clear:both;"></div>
</div></div>
<div id="social-media"><div class="inner">
	<a href="https://www.facebook.com/pages/Lori-Harders-Busy-Girl-Healthy-Life/110046032392720" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/facebook.png"  /></a>
	<a href="https://twitter.com/loriharder" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/twitter.png"  /></a>
	<a href="http://www.pinterest.com/loriharder/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/pinterest.png"  /></a>
	<a href="http://www.instagram.com/loriharder/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/instagram.png"  /></a>
</div></div>
   	


	