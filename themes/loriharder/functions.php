<?php 

require_once (ABSPATH . WPINC . '/class-snoopy.php');

if ( function_exists('register_sidebars') )
	register_sidebar(array('name'=>'Sidebar','before_widget'=>'<div class="widget">','after_widget'=>'</div>','before_title'=>'<h2>','after_title'=>'</h2>'));
	register_sidebar(array('name'=>'Workouts Sidebar','before_widget'=>'<div class="widget">','after_widget'=>'</div>','before_title'=>'<h2>','after_title'=>'</h2>'));
	register_sidebar(array('name'=>'Podcasts Sidebar','before_widget'=>'<div class="widget">','after_widget'=>'</div>','before_title'=>'<h2>','after_title'=>'</h2>'));
	register_sidebar(array('name'=>'Vlogs Sidebar','before_widget'=>'<div class="widget">','after_widget'=>'</div>','before_title'=>'<h2>','after_title'=>'</h2>'));


function featuredtoRSS($content) {global $post;
if ( has_post_thumbnail( $post->ID ) ){
$content = '' . get_the_post_thumbnail( $post->ID, 'full', array( 'style' => 'margin:10px auto;display:block;' ) ) . '' . $content;
}
return $content;
}

add_filter('the_excerpt_rss', 'featuredtoRSS');
add_filter('the_content_feed', 'featuredtoRSS');

function my_post_queries( $query ) {
  if (!is_admin() && $query->is_main_query()){

    if(is_category('17')){
      $query->set('posts_per_page', 5);
    }
    if(is_category('9')){
      $query->set('posts_per_page', 5);
    }
  }
}
add_action( 'pre_get_posts', 'my_post_queries' );

add_action('init', 'my_init_function');
 
function my_init_function() {
        add_image_size( 'recipes_image', 235, 185, TRUE ); 
}

function use_parent_category_template() {

    if ( !is_category() || is_404() ) return; // we only care about category views

    $cat = get_query_var('cat');
    $category = get_category ($cat);
      
    if ( !($category) ) return; // no category with which to work

        if ( file_exists(TEMPLATEPATH . '/category-' . $category->cat_ID . '.php') ) {
            include(TEMPLATEPATH . '/category-' . $category ->cat_ID . '.php');
            exit; }
        elseif ( file_exists(TEMPLATEPATH . '/category-' . $category->category_parent . '.php') ) {
            include(TEMPLATEPATH . '/category-' . $category->category_parent . '.php');
            exit; }
}
add_action('template_redirect', 'use_parent_category_template');


// comment callback function
function commentlist($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment ?>
	<li <?php comment_class('comment'); ?> id="comment-<?php comment_ID() ?>"><div id="div-comment-<?php comment_ID(); ?>" class="comments">
		<p class="comments-block">
			<strong class="comment-author"><?php comment_author_link() ?> </strong>
			<span class="comment-time comment-meta commentmetadata"><a class="date" href="#comment-<?php comment_ID(); ?>" title="Permanent Link to this comment"><?php comment_date('m/d/y'); echo " "; ?></a> <?php edit_comment_link('Edit','  (',')'); ?></span>
		</p>
		<div class="comment-body">
			<?php comment_text() ?> 
			<?php if ($comment->comment_approved == '0') : ?>
				<p>Thank you. Comments are moderated. Your comment will appear shortly.</p>
			<?php endif; ?>
		</div>
		<div class="reply"><?php comment_reply_link(array_merge( $args, array('add_below' => 'div-comment', 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?></div>
	</div>
<?php }
// end comment callback function (note: no need to close with </li>)

register_nav_menus( array(  
'primary' => __( 'Main Navigation', 'loriharder' )
) );

add_theme_support('post-thumbnails');

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}


?>