<?php
/*
Template Name: Praise
*/
?>
<?php get_header(); ?>

<div id="header-image" class="praise">
	<img src="<?php the_field('header_image'); ?>">
	<div class="inner"><div class="text"><?php the_field('header_title'); ?><span><?php the_field('header_subtitle'); ?></span></div></div>
	<div style="clear:both;"></div>
</div>
<?php include (TEMPLATEPATH . '/newsletter_optin.php'); ?>

<div id="outer-wrapper">
	<div id="main-wrapper" class="praise">

		<ul class="beforeafter">
		<?php if ( get_field('before_after1') ) : while ( has_sub_field('before_after1') ) : ?>
			<li class="baitem">
				<div class="left">
					<img src="<?php the_sub_field('before_image'); ?>">
					<img src="<?php the_sub_field('after_image'); ?>">
				</div>
				<div class="right">
					<div class="title"><?php the_sub_field('title'); ?></div>
					<div class="text"><?php the_sub_field('text'); ?></div>
				</div>
				<div style="clear: both;"></div>
			</li>
		<?php endwhile; endif; ?>
		</ul>

		<div class="border"></div>

		<ul class="testimonials">
		<?php if ( get_field('testimonials') ) : while ( has_sub_field('testimonials') ) : ?>
			<li class="testimonial">
				<div class="left">
					<img src="<?php the_sub_field('image'); ?>">
				</div>
				<div class="right">
					<div class="title"><?php the_sub_field('title'); ?></div>
					<div class="text"><?php the_sub_field('content'); ?></div>
				</div>
				<div style="clear: both;"></div>
			</li>
		<?php endwhile; endif; ?>
		</ul>

		<div class="border"></div>

		<ul class="beforeafter">
		<?php if ( get_field('before_after2') ) : while ( has_sub_field('before_after2') ) : ?>
			<li class="baitem">
				<div class="left">
					<img src="<?php the_sub_field('before_image'); ?>">
					<img src="<?php the_sub_field('after_image'); ?>">
				</div>
				<div class="right">
					<div class="title"><?php the_sub_field('title'); ?></div>
					<div class="text"><?php the_sub_field('text'); ?></div>
				</div>
				<div style="clear: both;"></div>
			</li>
		<?php endwhile; endif; ?>
		</ul>

		<div class="border"></div>

		<ul class="testimonials">
		<?php if ( get_field('testimonials2') ) : while ( has_sub_field('testimonials2') ) : ?>
			<li class="testimonial">
				<div class="left">
					<img src="<?php the_sub_field('image'); ?>">
				</div>
				<div class="right">
					<div class="title"><?php the_sub_field('title'); ?></div>
					<div class="text"><?php the_sub_field('content'); ?></div>
				</div>
				<div style="clear: both;"></div>
			</li>
		<?php endwhile; endif; ?>
		</ul>

		<div class="border"></div>

		<ul class="beforeafter">
		<?php if ( get_field('before_after3') ) : while ( has_sub_field('before_after3') ) : ?>
			<li class="baitem">
				<div class="left">
					<img src="<?php the_sub_field('before_image'); ?>">
					<img src="<?php the_sub_field('after_image'); ?>">
				</div>
				<div class="right">
					<div class="title"><?php the_sub_field('title'); ?></div>
					<div class="text"><?php the_sub_field('text'); ?></div>
				</div>
				<div style="clear: both;"></div>
			</li>
		<?php endwhile; endif; ?>
		</ul>

   	</div>

     <div style="clear: both;"></div>
</div></div>
<?php get_footer(); ?>