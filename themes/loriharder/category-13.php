<?php get_header(); ?>

<div id="header-image" class="recipes">
	<img src="<?php the_field('header_image', '183'); ?>">
	<div class="inner"><div class="text"><?php the_field('header_title', '183'); ?><span><?php the_field('header_subtitle', '183'); ?></span></div></div>
	<div style="clear:both;"></div>
</div>
<?php include (TEMPLATEPATH . '/newsletter_optin.php'); ?>

<div id="blog-wrapper" class="recipespage">
<?php if (have_posts()) : ?>
<h1><?php echo single_cat_title(); ?></h1>
<div id="post-wrapper">
	<ul class="therecipes">
	<?php while (have_posts()) : the_post(); ?>	
		<li id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="thumbnail"><?php the_post_thumbnail('recipes_image'); ?></div>
               		<h3><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
		</li>
	<?php endwhile; ?>
	<div class="navigation">
		<div class="blog-pager-older-link"><?php next_posts_link('<< More Recipes') ?></div>
		<div class="blog-pager-newer-link"><?php previous_posts_link('More Recipes >>') ?></div>
	</div>
	</ul>				
</div>	
<?php endif; ?>
<div id="sidebar-wrapper">
	<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Sidebar')) : ?><?php endif; ?>
</div>
<div style="clear: both;"></div>

</div>

<?php get_footer(); ?>	