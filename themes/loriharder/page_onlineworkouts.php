<?php
/*
Template Name: Online Workouts
*/
?>
<?php get_header(); ?>

<div id="header-image" class="onlineworkouts">
	<img src="<?php the_field('header_image'); ?>">
	<div class="inner"><div class="text"><?php the_field('header_title'); ?><span><?php the_field('header_subtitle'); ?></span></div></div>
	<div style="clear:both;"></div>
</div>
<?php include (TEMPLATEPATH . '/newsletter_optin.php'); ?>


<div id="main-wrapper" class="onlineworkouts">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="left">
			<h1><?php the_field('program_title'); ?></h1>
			<div class="text1"><?php the_field('program_content'); ?></div>
			<h1 class="second"><?php the_field('cycle_title'); ?></h1>
			<div class="text1"><?php the_field('cycle_content'); ?></div>
		</div>

		<div class="right">
			<img src="<?php the_field('workout_image'); ?>">
			<div class="box">
				<div class="top"><?php the_field('includes_title'); ?></div>
				<div class="bottom"><?php the_field('includes_content'); ?></div>
			</div>
		</div>

		<div style="clear: both;"></div>
         
	</div>
	<?php endwhile; endif; ?>				
					
		</div>	
</div>

     <div style="clear: both;"></div>
</div></div>
<?php get_footer(); ?>