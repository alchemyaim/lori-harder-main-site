<?php
  $post = $wp_query->post;
  if (in_category('workout-videos')) {
      include(TEMPLATEPATH.'/single-17.php');
  } elseif (in_category('healthy-minds')) {
      include(TEMPLATEPATH.'/single-9.php');
  } elseif(in_category('recipes')) {
      include(TEMPLATEPATH.'/single-13.php');
  }
  else{
      include(TEMPLATEPATH.'/single-default.php');
  }
?>