    <div style="clear:both;"></div>
<div id="credits"><div class="inner">

	<div class="left">Copyright &#169; <?php echo date('Y'); ?> Lori Harder | Designed with Love by <a href="http://www.rachelpesso.com" target="_blank">Rachel Pesso</a> | Developed by <a href="http://coding.brandibernoskie.com/" target="_blank">Brandi Bernoskie</a></div>
	<div class="right"><a href="/terms-and-conditions">Terms + Conditions</a> | <a href="/privacy-policy">Privacy Policy</a>  | <a href="/contact">Contact</a></div>
	 <div style="clear:both;"></div>
	<?php wp_footer(); ?>
</div></div>
</div>
</body>
</html>