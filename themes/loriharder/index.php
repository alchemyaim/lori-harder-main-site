<?php get_header(); ?>	
		<div id="main-wrapper">
        <div style="clear: both;"></div>
        <div id="main" class="main section">
		<?php if (have_posts()) : ?>			
			<?php while (have_posts()) : the_post(); ?>
			
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
               			<h1><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></h1>
            			<h4 class="date-header"><?php the_time(get_option('date_format')); ?></h4>

				<div class="post-body entry-content">
					<?php the_content('READ MORE'); ?>
				</div>	
				<div class="post-footer">
                             <div class="comment-link"><?php if ( comments_open() ) : comments_popup_link( '0 comments', '1 comment', '% comments', '', ''); endif; ?></div>
							<div class="share-buttons">
                                	<div class="pinterest-share-button"><a href="//www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark" ><img src="<?php bloginfo('template_url'); ?>/images/share_pinterest.png"  /></a></div>
                                	<div class="twitter-share-button"><a href="http://twitter.com/home?status=Currently reading: <?php the_title ();?> <?php echo get_settings('home'); ?>/?p=<?php the_ID(); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/share_twitter.png"  /></a></div>
                                	<div class="fb-share-button"><a href="http://www.facebook.com/sharer.php?u=<?php the_permalink() ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/share_facebook.png"  /></a></div>
                             </div>
				</div>
			</div>


			<?php endwhile; ?>			
			<?php include (TEMPLATEPATH . '/navigation.php'); ?>			
		<?php else : ?>	
			<h3 class="page_header center">Not Found</h3>
			<div class="post-body entry-content">
				<p class="center">Sorry, but you are looking for something that isn't here.</p>
				<?php get_search_form(); ?>
			</div>	
		<?php endif; ?>		
		</div>
        </div>
    </div>
<?php get_footer(); ?>