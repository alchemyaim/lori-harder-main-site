<?php
/*
Template Name: Contact
*/
?>
<?php get_header(); ?>

<div id="header-image" class="contact">
	<img src="<?php the_field('header_image'); ?>">
	<div class="inner"><div class="text"><?php the_field('header_title'); ?></div></div>
	<div style="clear:both;"></div>
</div>
<?php include (TEMPLATEPATH . '/newsletter_optin.php'); ?>


<div id="main-wrapper" class="contact">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<h1><?php the_field('page_title'); ?></h1>
		<div class="left">
			<?php the_content(); ?>
		</div>	

		<div class="right">
			<img src="<?php the_field('contact_image'); ?>">
		</div>

		<div style="clear: both;"></div>
         
	</div>
	<?php endwhile; endif; ?>				
					
		</div>	
</div>

     <div style="clear: both;"></div>
</div></div>
<?php get_footer(); ?>