<?php
/*
Template Name: Free Workouts
*/
?>
<?php get_header(); ?>

<div id="header-image" class="freeworkouts">
	<img src="<?php the_field('header_image'); ?>">
	<div class="inner"><div class="text"><?php the_field('header_title'); ?><span><?php the_field('header_subtitle'); ?></span></div></div>
	<div style="clear:both;"></div>
</div>
<?php include (TEMPLATEPATH . '/newsletter_optin.php'); ?>

<div id="blog-wrapper"><div id="freeworkouts">
	<div id="main-wrapper">

		<ul class="freevideos">
		<?php if ( get_field('videos') ) : while ( has_sub_field('videos') ) : ?>
			<li class="video">
			<div class="title"><?php the_sub_field('title'); ?></div>
			<div class="content"><?php the_sub_field('content'); ?></div>
			</li>
		<?php endwhile; endif; ?>
		</ul>

   	</div>

     <div style="clear: both;"></div>
</div></div>
<?php get_footer(); ?>