<?php
/*
Template Name: Cookbook
*/
?>
<?php get_header(); ?>

<div id="header-image" class="cookbook">
	<img src="<?php the_field('header_image'); ?>">
	<div class="inner"><div class="text"><?php the_field('header_title'); ?><span><?php the_field('header_subtitle'); ?></span></div></div>
	<div style="clear:both;"></div>
</div>
<?php include (TEMPLATEPATH . '/newsletter_optin.php'); ?>

<div id="outer-wrapper"><div id="cookbook">
	<div id="main-wrapper">
		<div class="information">
			<div class="left">
				<h2><?php the_field('cookbook_title'); ?></h2>
				<div class="text"><?php the_field('cookbook_text'); ?></div>
				<a href="<?php the_field('buy_link'); ?>" target="_blank" class="buy"><?php the_field('buynow_text'); ?></a><a href="<?php the_field('download_link'); ?>" target="_blank"  class="download"><?php the_field('downloadnow_text'); ?></a>
			</div>
			<div class="right">
				<img src="<?php the_field('cookbook_image'); ?>">
			</div>
			<div style="clear:both;"></div>
		</div>

		<ul class="testimonials">
		<?php if ( get_field('testimonials') ) : while ( has_sub_field('testimonials') ) : ?>
			<li class="testimonial">
			<img src="<?php the_sub_field('image'); ?>">
			<div class="title"><?php the_sub_field('title'); ?></div>
			<div class="quote"><span class="mark1">“</span><?php the_sub_field('quote'); ?><span class="mark2">“</span></div>
			<div class="name"><?php the_sub_field('name'); ?></div>
			</li>
		<?php endwhile; endif; ?>
		</ul>

   	</div>

	<div class="recipe"><div class="inner">
		<h3><?php the_field('recipe_title'); ?></h3>
		<img src="<?php the_field('recipe_image'); ?>">
		<div class="text"><?php the_field('recipe_text'); ?></div>
	</div></div>
	<div class="recipebottom"><div class="inner">
		<div class="left"><?php the_field('recipe_nutrition'); ?></div>
		<div class="right"><a href="<?php the_field('buy_link'); ?>" target="_blank"  class="buy"><?php the_field('buynow_button2'); ?></a><a href="<?php the_field('download_link'); ?>" target="_blank"  class="download"><?php the_field('downloadnow_button2'); ?></a></div>
		<div style="clear: both;"></div>
	</div></div>

     <div style="clear: both;"></div>
</div></div>
<?php get_footer(); ?>