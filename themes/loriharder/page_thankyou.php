<?php
/*
Template Name: Thank You
*/
?>
<?php get_header(); ?>

<div id="header-image" class="thankyou">
	<img src="<?php the_field('header_image'); ?>">
	<div class="inner"><div class="text"><?php the_field('header_title'); ?><span><?php the_field('header_text'); ?></span></div></div>
	<div style="clear:both;"></div>
</div>

<div id="main-wrapper">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="page-body">
			<?php the_content('READ MORE'); ?>
		</div>	
         
	</div>
	<?php endwhile; endif; ?>				
</div>


     <div style="clear: both;"></div>
</div></div>
<?php get_footer(); ?>