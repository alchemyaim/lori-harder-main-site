<?php
/*
Template Name: About
*/
?>
<?php get_header(); ?>

<div id="header-image" class="about">
	<img src="<?php the_field('header_image'); ?>">
	<div class="inner"><div class="text"><?php the_field('header_title'); ?><span><?php the_field('header_subtitle'); ?></span></div></div>
	<div style="clear:both;"></div>
</div>
<?php include (TEMPLATEPATH . '/newsletter_optin.php'); ?>


<div id="main-wrapper" class="about">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<h1><?php the_field('page_title'); ?></h1>
		<div class="left">
			<?php the_content(); ?>
		</div>	
		<div class="right">
			<?php the_field('right_images'); ?>
		</div>
		<div style="clear:both;"></div>
	
		<div class="magazines">
			<img src="<?php the_field('magazine_covers'); ?>">
		</div>
		<div class="bio">
			<h3><?php the_field('bio_title'); ?></h3>
			<div class="text"><?php the_field('bio_content'); ?></div>
		</div>
         
	</div>
	<?php endwhile; endif; ?>				
					
		</div>	
</div>

     <div style="clear: both;"></div>
</div></div>
<?php get_footer(); ?>