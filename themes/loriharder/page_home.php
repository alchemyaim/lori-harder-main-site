<?php
/*
Template Name: Home
*/
?>
<?php get_header(); ?>

<div id="header-image" class="home">
	<img src="<?php the_field('home_header'); ?>">
	<div class="inner"><div class="text"><?php the_field('header_title'); ?><span><?php the_field('header_subtitle'); ?></span></div></div>
	<div style="clear:both;"></div>
</div>
<?php include (TEMPLATEPATH . '/newsletter_optin.php'); ?>

<div id="homepage">
	<div id="main-wrapper">
		<div class="manifesto">
			<h2><?php the_field('manifesto_title'); ?></h2>
			<div class="text"><?php the_field('manifesto_text'); ?></div>
		</div>
   	</div>

	<div class="asseen"><div id="main-wrapper">
		<h3><?php the_field('seen_title'); ?></h3>
		<div class="gallery"><?php the_field('seen_images'); ?></div>
	</div></div>

	<div class="mainlinks">
		<div class="body"><a href="/body/online-workouts/">
			<img src="<?php the_field('body_image'); ?>">
			<div class="box"><?php the_field('body_title'); ?><span><?php the_field('body_subtitle'); ?></span></div>
		</a></div>
		<div class="mind"><a href="/healthy-minds/">
			<img src="<?php the_field('mind_image'); ?>">
			<div class="box"><?php the_field('mind_title'); ?><span><?php the_field('mind_subtitle'); ?></span></div>
		</a></div>
		<div style="clear: both;"></div>
		<div class="nourish"><a href="/nourish/cookbook/">
			<img src="<?php the_field('nourish_image'); ?>">
			<div class="box"><?php the_field('nourish_title'); ?><span><?php the_field('nourish_subtitle'); ?></span></div>
		</a></div>
	</div>

	<div class="video"><div id="main-wrapper">
		<div class="video-area">
			<?php $args=array( 'posts_per_page'=>1,'post_type'=>'post', 'cat'=>'9');
 			$my_query = new WP_Query($args); if( $my_query->have_posts() ) { 
			while ($my_query->have_posts()) : $my_query->the_post(); ?>
				<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full'); ?></a>
			<?php endwhile; } wp_reset_postdata(); ?>
		</div>
		<div class="video-info"><?php the_field('video_title'); ?><span><?php the_field('video_subtitle'); ?></span></div>
		<div style="clear: both;"></div>
	</div></div>

	<div class="home-about" id="main-wrapper">
		<div class="left"><img src="<?php the_field('meet_image'); ?>"></div>
		<div class="right">
			<h2><?php the_field('meet_title'); ?></h2>
			<div class="text"><?php the_field('meet_text'); ?></div>
			<a href="/meet-lori" class="read-more">Read More!</a>
		</div>
		<div style="clear: both;"></div>
	</div>

	<div class="goodies"><div id="main-wrapper">
		<h3><?php the_field('goodie_title'); ?><span><?php the_field('goodie_subtitle'); ?></span></h3>
		<ul class="thegoodies">
		<?php if ( get_field('goodies') ) : while ( has_sub_field('goodies') ) : ?>
			<li class="goodie">
			<a href="<?php the_sub_field('goodie_link'); ?>"><img src="<?php the_sub_field('goodie_image'); ?>"><div class="text"><?php the_sub_field('goodie_text'); ?></div></a>
			</li>
		<?php endwhile; endif; ?>
		</ul>
	</div></div>

     <div style="clear: both;"></div>
</div></div>
<?php get_footer(); ?>