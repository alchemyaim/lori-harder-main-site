<link href='http://fonts.googleapis.com/css?family=Roboto:400,700&subset=cyrillic,latin,latin-ext' rel='stylesheet' type='text/css'>
<?php
/* Set Defaults */
	$fields['bg_end'] = !empty($fields['bg_end']) ? $fields['bg_end'] : '#0e7e9c';
	$fields['bg_start'] = !empty($fields['bg_start']) ? $fields['bg_start'] : '#004f75';
	$bgs = $fields['bg_start'];
	$bge = $fields['bg_end'];
?>

<style>
	.lightbox-main {
	background: <?php echo $bgs; ?>;
	background: -moz-radial-gradient(center, ellipse cover, <?php echo $bge ?> 0%, <?php echo $bge ?> 0%, <?php echo $bgs ?> 100%);
	background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%, <?php echo $bge ?>), color-stop(0%, <?php echo $bge ?>), color-stop(100%, <?php echo $bgs ?>;));
	background: -webkit-radial-gradient(center, ellipse cover, <?php echo $bge ?> 0%, <?php echo $bge ?> 0%, <?php echo $bgs ?> 100%);
	background: -o-radial-gradient(center, ellipse cover, <?php echo $bge ?> 0%, <?php echo $bge ?> 0%, <?php echo $bgs ?> 100%);
	background: -ms-radial-gradient(center, ellipse cover, <?php echo $bge ?> 0%, <?php echo $bge ?> 0%, <?php echo $bgs ?> 100%);
	background: radial-gradient(ellipse at center, <?php echo $bge ?> 0%, <?php echo $bge ?> 0%, <?php echo $bgs ?> 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#255c74', endColorstr='<?php echo $bgs ?>', GradientType=1 );
	}
	.popup3 .topline, .popup3 .footer {
	background: <?php echo $bgs; ?>; /* Old browsers */
	background: -moz-linear-gradient(-45deg,  <?php echo $bge; ?> 0%, <?php echo $bgs; ?> 35%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, right bottom, color-stop(0%,<?php echo $bge; ?>), color-stop(35%,<?php echo $bgs; ?>)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(-45deg,  <?php echo $bge; ?> 0%,<?php echo $bgs; ?> 35%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(-45deg,  <?php echo $bge; ?> 0%,<?php echo $bgs; ?> 35%); /* Opera 11.10+ */
	background: -ms-linear-gradient(-45deg,  <?php echo $bge; ?> 0%,<?php echo $bgs; ?> 35%); /* IE10+ */
	background: linear-gradient(135deg,  <?php echo $bge; ?> 0%,<?php echo $bgs; ?> 35%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='<?php echo $bge; ?>', endColorstr='<?php echo $bgs; ?>',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
	}
	.popup3 span {
		color: <?php echo $fields['font_first'] ?>;
	}
	.image-block {
		color: <?php echo $fields['font_first'] ?>;
	}
	.image-block h1 {
		color: <?php echo $fields['font_first'] ?>;
	}
	.image-block .detail p {
		color: <?php echo $fields['font_second'] ?>;
	}
	.popup3 .email-form input[type="submit"] {
		color: <?php echo $fields['button_font_color'] ?>;
		background: <?php echo $fields['button_color'] ?>;
	}
	.popup3 .text {
		color: <?php echo $fields['font_first'] ?>;
	}
	.popup3 .par {
		color: <?php echo $fields['font_second'] ?>;
	}
	.checkmark, .checkmark path {
		fill: <?php echo $fields['button_color'] ?>;
	}
	.popup3 .secure-lock, .secure-lock path {
		fill: <?php echo $fields['font_first'] ?>;
	}
	.popup3 .action-text {
		color: <?php echo $fields['font_second'] ?>;
	}
	.popup3 .yes_button h2, .popup3 .yes_button p{
		color: <?php echo $fields['yes_button_font_color'] ?>;
	}
	.popup3 .yes_button {
		background: <?php echo $fields['yes_button_color'] ?>;
	}
	.popup3 .no_button h2, .popup3 .no_button p{
		color: <?php echo $fields['no_button_font_color'] ?>;
	}
	.popup3 .no_button {
		background: <?php echo $fields['no_button_color'] ?>;
	}
</style>
<div class="popup-dom-lightbox-wrapper" id="<?PHP echo $lightbox_id?>"<?PHP echo $delay_hide ?>>
	<div class="lightbox-overlay"></div>
	<div style="width: 771px" class="lightbox-main lightbox-color-<?PHP echo $color ?> popup3">
		<div class="colorbox"></div>
		<div class="topline">
			<span><?php echo $fields['as_seen_text'] ?></span>
			<img class="as-seen-in" src="<?PHP if(!empty($fields['toplineimage'])) { echo $fields['toplineimage']; } else { echo $theme_url.'/images/as-seen-in.png'; } ?>" alt="">
		</div>
		<div class="image-block main">
			<h1><?php echo $fields['title']; ?></h1>
		</div>
		<div class="image-block">
			<div class="footer">
				<div>
					<a id="yes-button" class="yes_button" href="<?php echo $fields['yes_button_link']; ?>">
						<h2><?php echo $fields['yes_button'] ?></h2>
						<p><?php echo $fields['yes_button_text'] ?></p>
					</a>
					<a id="no-button" class="no_button" href="<?php echo $fields['no_button_link']; ?>">
						<h2><?php echo $fields['no_button'] ?></h2>
						<p><?php echo $fields['no_button_text'] ?></p>
					</a>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<a href="#" class="button-color-<?php echo $button_color ?> close lightbox-close" id="<?PHP echo $lightbox_close_id?>">&times;</a>
		<div style="position: absolute; width: 100%">
			<?php echo $promote_link ?>
		</div>
	</div>
</div>
<script type="text/javascript">
	var yesButton = window.frames[0].document.getElementById('yes-button');
	jQuery(yesButton).click(function(e){
		e.preventDefault();
		popup_domination.close_box(popup_domination.popupid, false);
		var link = jQuery(yesButton).attr('href');
		if(link && link != '#' && link != '') {
			window.parent.location.href = link;
		}
	});
	var noButton = window.frames[0].document.getElementById('no-button');
	jQuery(noButton).click(function(e){
		e.preventDefault();
		popup_domination.close_box(popup_domination.popupid, false);
		var link = jQuery(noButton).attr('href');
		if(link && link != '#' && link != '') {
			window.parent.location.href = link;
		}
	});
</script>