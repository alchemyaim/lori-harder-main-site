<link href='http://fonts.googleapis.com/css?family=Roboto:400,700&subset=cyrillic,latin,latin-ext' rel='stylesheet' type='text/css'>
<?php
/* Set Defaults */
	$fields['bg_end'] = !empty($fields['bg_end']) ? $fields['bg_end'] : '#0e7e9c';
	$fields['bg_start'] = !empty($fields['bg_start']) ? $fields['bg_start'] : '#004f75';
	$bgs = $fields['bg_start'];
	$bge = $fields['bg_end'];
?>

<style>
	.lightbox-main {
	background: <?php echo $bgs; ?>;
	background: -moz-radial-gradient(center, ellipse cover, <?php echo $bge ?> 0%, <?php echo $bge ?> 0%, <?php echo $bgs ?> 100%);
	background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%, <?php echo $bge ?>), color-stop(0%, <?php echo $bge ?>), color-stop(100%, <?php echo $bgs ?>;));
	background: -webkit-radial-gradient(center, ellipse cover, <?php echo $bge ?> 0%, <?php echo $bge ?> 0%, <?php echo $bgs ?> 100%);
	background: -o-radial-gradient(center, ellipse cover, <?php echo $bge ?> 0%, <?php echo $bge ?> 0%, <?php echo $bgs ?> 100%);
	background: -ms-radial-gradient(center, ellipse cover, <?php echo $bge ?> 0%, <?php echo $bge ?> 0%, <?php echo $bgs ?> 100%);
	background: radial-gradient(ellipse at center, <?php echo $bge ?> 0%, <?php echo $bge ?> 0%, <?php echo $bgs ?> 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#255c74', endColorstr='<?php echo $bgs ?>', GradientType=1 );
	}
	.popup3 .topline, .popup3 .footer {
	background: <?php echo $bgs; ?>; /* Old browsers */
	background: -moz-linear-gradient(-45deg,  <?php echo $bge; ?> 0%, <?php echo $bgs; ?> 35%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, right bottom, color-stop(0%,<?php echo $bge; ?>), color-stop(35%,<?php echo $bgs; ?>)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(-45deg,  <?php echo $bge; ?> 0%,<?php echo $bgs; ?> 35%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(-45deg,  <?php echo $bge; ?> 0%,<?php echo $bgs; ?> 35%); /* Opera 11.10+ */
	background: -ms-linear-gradient(-45deg,  <?php echo $bge; ?> 0%,<?php echo $bgs; ?> 35%); /* IE10+ */
	background: linear-gradient(135deg,  <?php echo $bge; ?> 0%,<?php echo $bgs; ?> 35%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='<?php echo $bge; ?>', endColorstr='<?php echo $bgs; ?>',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
	}
	.popup3 span {
		color: <?php echo $fields['font_first'] ?>;
	}
	.image-block {
		color: <?php echo $fields['font_first'] ?>;
	}
	.image-block h2 {
		color: <?php echo $fields['font_first'] ?>;
	}
	.image-block .detail p {
		color: <?php echo $fields['font_second'] ?>;
	}
	.popup3 .email-form input[type="submit"] {
		color: <?php echo $fields['button_font_color'] ?>;
		background: <?php echo $fields['button_color'] ?>;
	}
	.popup3 .text {
		color: <?php echo $fields['font_first'] ?>;
	}
	.popup3 .par {
		color: <?php echo $fields['font_second'] ?>;
	}
	.checkmark, .checkmark path {
		fill: <?php echo $fields['button_color'] ?>;
	}
	.secure-lock, .secure-lock path {
		fill: <?php echo $fields['font_first'] ?>;
	}
	.popup3 .action-text {
		color: <?php echo $fields['font_second'] ?>;
	}
</style>
<div class="popup-dom-lightbox-wrapper" id="<?PHP echo $lightbox_id?>"<?PHP echo $delay_hide ?>>
	<div class="lightbox-overlay"></div>
	<div style="width: 771px" class="lightbox-main lightbox-color-<?PHP echo $color ?> popup3">
		<div class="colorbox"></div>
		<div class="topline">
			<span><?php echo $fields['as_seen_text'] ?></span>
			<img class="as-seen-in" src="<?PHP if(!empty($fields['toplineimage'])) { echo $fields['toplineimage']; } else { echo $theme_url.'/images/as-seen-in.png'; } ?>" alt="">
		</div>
		<div class="image-block main">
			<div class="image-holder">
				<img class="main-image" src="<?PHP if(!empty($fields['image'])) { echo $fields['image']; } else { echo $theme_url.'/images/default.png'; } ?>" alt="">
			</div>
			<div class="detail">
				<h1 class="par"><?PHP echo nl2br($fields['title']) ?></h1>
        <ul class="bullet-list"><?php
            $count = 1;
            if(isset($list_items) && count($list_items) > 0):
                foreach($list_items as $l):
                    if($count>3)
                        break;?>
            <li>
            	<svg class="checkmark" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="20px" height="20px" viewBox="0 0 512 512" enable-background="new 0 0 512 512" xml:space="preserve">
								<polygon id="check-mark-icon" points="398.218,92.985 199.729,291.475 113.754,205.476 50,269.242 199.733,419.015 462,156.726 "/>
							</svg>
            <?php echo $l ?></li><?php
                    $count++;
                endforeach;
            endif;?>
        </ul>
			</div>
		</div>
		<div class="image-block">
			<div class="footer">
				<form method="post" action="<?php echo $form_action ?>"<?php echo $target ?> class="email-form">
					<span class="action-text"><?php echo $fields['action_text']; ?></span>
					<?PHP echo $inputs['hidden'].$fstr; ?>
					<input class="button-color-<?php echo $button_color ?>" type="submit" value="<?PHP echo $fields['submit_button'] ?>">
				</form>
				<div class="clear"></div>
				<span class="text">
					<svg class="secure-lock" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 512 512" enable-background="new 0 0 512 512" xml:space="preserve">
					<path id="lock-icon" d="M145.333,223.333v-62.666C145.333,99.645,194.979,50,256,50c61.022,0,110.667,49.645,110.667,110.667v62.666
						h-50v-62.666C316.667,127.215,289.452,100,256,100c-33.452,0-60.667,27.215-60.667,60.667v62.666H145.333z M108,253.333V462h296
						V253.333H108z"/>
					</svg>
				<?PHP echo $fields['footer_note'] ?></span>
			</div>
		</div>
		<a href="#" class="button-color-<?php echo $button_color ?> close lightbox-close" id="<?PHP echo $lightbox_close_id?>">&times;</a>
		<div style="position: absolute; width: 100%">
			<?php echo $promote_link ?>
		</div>
	</div>
</div>