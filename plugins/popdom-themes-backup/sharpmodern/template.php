<link href='http://fonts.googleapis.com/css?family=Didact+Gothic&subset=cyrillic,cyrillic-ext,latin,latin-ext' rel='stylesheet' type='text/css'>
<style>
	.lightbox-main {
		background: <?php echo $fields['bg_start'] ?>; /* Old browsers */
		background: -moz-linear-gradient(-45deg, <?php echo $fields['bg_start'] ?> 0%, <?php echo $fields['bg_end'] ?> 100%); /* FF3.6+ */
		background: -webkit-gradient(linear, left top, right bottom, color-stop(0%,<?php echo $fields['bg_start'] ?>), color-stop(100%,#ffffff));
		background: -webkit-linear-gradient(-45deg, <?php echo $fields['bg_start'] ?> 0%,<?php echo $fields['bg_end'] ?> 100%); /* Chrome10+,Safari5.1+ */
		background: -o-linear-gradient(-45deg, <?php echo $fields['bg_start'] ?> 0%,<?php echo $fields['bg_end'] ?> 100%); /* Opera 11.10+ */
		background: -ms-linear-gradient(-45deg, <?php echo $fields['bg_start'] ?> 0%,<?php echo $fields['bg_end'] ?> 100%); /* IE10+ */
		background: linear-gradient(135deg, <?php echo $fields['bg_start'] ?> 0%,<?php echo $fields['bg_end'] ?> 100%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='<?php echo $fields["bg_start"] ?>', endColorstr='<?php echo $fields["bg_end"] ?>',GradientType=1 );
	}
	.image-block .detail h2 {
		color: <?php echo $fields['font_first'] ?>;
	}
	.image-block .detail p {
		color: <?php echo $fields['font_second'] ?>;
	}
	.popup3 .email-form input[type="submit"] {
		background: <?php echo $fields['button_color'] ?>;
	}
	.popup3 .text {
		color: <?php echo $fields['font_second'] ?>;
	}
</style>
<div class="popup-dom-lightbox-wrapper" id="<?PHP echo $lightbox_id?>"<?PHP echo $delay_hide ?>>
	<div class="lightbox-overlay"></div>
	<div style="width: 771px" class="lightbox-main lightbox-color-<?PHP echo $color ?> popup3">
		<div class="image-block">
			<div class="image-holder"> 
				<img src="<?PHP if(!empty($fields['image'])) { echo $fields['image']; } else { echo $theme_url.'/images/placeholder.png'; } ?>" alt="">
			</div>
			<div class="detail">
				<h2><?php echo $fields['title'] ?></h2>
				<p><?PHP echo nl2br($fields['short_paragraph']) ?></p>
				<form method="post" action="<?php echo $form_action ?>"<?php echo $target ?> class="email-form">
					<?PHP echo $inputs['hidden'].$fstr; ?>
					<span class="email-placeholder"><?php echo $fields['email_label'] ?></span>
					<input class="button-color-<?php echo $button_color ?>" type="submit" value="<?PHP echo $fields['submit_button'] ?>">
				</form>
				<span class="text"><span class="icon-secure"></span><?PHP echo $fields['footer_note'] ?></span>
			</div>
		</div>
		<a href="#" class="button-color-<?php echo $button_color ?> close lightbox-close" id="<?PHP echo $lightbox_close_id?>">&times;</a>
	</div>
</div>