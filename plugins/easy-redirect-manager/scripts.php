<?php

class wps_rm_scripts extends wps_rm {
  
  
  static function admin() {
    $current = get_current_screen();
    
    if (is_admin() || $current->base == 'toplevel_page_wps-redirect-manager' || $current->base == 'toplevel_page_wps-redirect-log') {
      wp_enqueue_style(WPS_RM . '-css', plugins_url('css/admin.css', __FILE__), '', wps_rm::$version);
      wp_enqueue_script(WPS_RM . '-js', plugins_url('js/admin.js', __FILE__), array('jquery'), wps_rm::$version);
    }
    
  } // menu
  
  
} // wps_rm_scripts