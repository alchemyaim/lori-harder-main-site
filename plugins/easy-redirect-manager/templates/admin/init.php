<div class="wps-header-bar">
  <h1>Redirect Manager</h1>
  <div class="wps-header-bar-right">
    <a href="mailto:support@premiumwpsuite.com">Support <i class="dashicons dashicons-email"></i></a>
    <a href="mailto:hireus@premiumwpsuite.com">Hire us <i class="dashicons dashicons-wordpress"></i></a>
  </div>
</div>


<div class="wrap">

  <?php wps_rm_pages::redirects_table(); ?>

</div>