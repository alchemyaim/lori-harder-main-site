<div class="wps-header-bar">
  <h1>Redirect Manager <a href="#" id="wps-erm-clear-logs" class="button button-primary">Clear Log</a></h1>
  <div class="wps-header-bar-right">
    <a href="mailto:support@premiumwpsuite.com">Support <i class="dashicons dashicons-email"></i></a>
    <a href="mailto:hireus@premiumwpsuite.com">Hire us <i class="dashicons dashicons-wordpress"></i></a>
  </div>
</div>


<div class="wrap">
<table id="wps-redirects-log" class="wp-list-table widefat fixed striped">
  <thead>
    <tr>
      <th>Source URL</th>
      <th>Target URL</th>
      <th>Redirect Code</th>
      <th>User Ref.</th>
      <th>User Agent</th>
      <th>User IP</th>
      <th>Timestamp</th>
    </tr>
  </thead>

  <tbody>
    <?php
    global $wpdb;
    $redirects = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "rm_log ORDER by ID DESC");
    if ($redirects) {
      foreach ($redirects as $redirect) {
        
        if (!empty($redirect->user_referral)) {
          $ref = $redirect->user_referral;
        } else {
          $ref = 'Uknown';
        }
        
        ?>
        <tr>
          <td><a href="<?php echo site_url($redirect->source); ?>"><?php echo $redirect->source; ?></a></td>
          <td><?php echo $redirect->target; ?></td>
          <td><?php echo $redirect->code; ?></td>
          <td><?php echo $ref; ?></td>
          <td><?php echo $redirect->user_agent; ?></td>
          <td><?php echo $redirect->user_ip; ?></td>
          <td><?php echo date('d.m.Y H:i:m', strtotime($redirect->timestamp)); ?></td>
        </tr>
        <?php
      }
    } else {
      ?>
      <tr id="no-results">
        <td colspan="7" style="text-align: center;">
          <strong>No redirects found.</strong><br/><br/>
        </td>
      </tr>
      <?php
    }
    ?>
  </tbody>

  <tfoot>
    <tr>
      <th>Source URL</th>
      <th>Target URL</th>
      <th>Redirect Code</th>
      <th>User Ref.</th>
      <th>User Agent</th>
      <th>User IP</th>
      <th>Timestamp</th>
    </tr>
  </tfoot>
</table>
</div>