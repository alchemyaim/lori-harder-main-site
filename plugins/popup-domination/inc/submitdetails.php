<?PHP

$provider = $_POST['provider'];
$campaignID = $_POST['campaignid'];
$campname = $_POST['campname'];
$mailing_list_id = $_POST['mailingid'];

if($provider != 'form') {
    $mailing_info = unserialize(base64_decode($this->option('formhtml')));
    if(empty($mailing_info) || (!empty($mailing_info['listid']) && $mailing_info['listid'] != $_POST['listid'] )) {
        $mailinglists = $this->get_db('popdom_mailing');
        if(is_array($mailinglists)) {
            foreach($mailinglists as $mailinglist) {
                if($mailinglist->id == $mailing_list_id) {
                    $mailing_info = unserialize(base64_decode($mailinglist->settings));
                }
            }
        }
    }

    if(!empty($mailing_info) && !empty($provider)) {
        // set up variables used by every provider
        foreach($mailing_info as $key => $value) {
            $$key = $value;
        }

        if(gettype($provider_details) == 'array') {
            // create all variables required for each provider
            foreach($provider_details as $key => $value) {
                $$key = $value;
            }
            $formhtml = isset($provider_details['formhtml']) ? stripslashes($provider_details['formhtml']) : '';
            $hidden_fields = '';
            if(isset($hidden)) {
                foreach($hidden as $key => $value) {
                    $$key = $value;
                }
            }
        }
    }

    $provider = $_POST['provider'];
}

if(!empty($_POST['mailnotify']) || $provider == 'nm') {
    $to = $bcc = '';
    $to = (!empty($_POST['master'])) ? $_POST['master'] : $_POST['mailnotify'];
    $bcc = (!empty($_POST['master'])) ? $_POST['mailnotify'] : '';
    $subject = "PopUp Domination Sign Up";
    $name_field = (!empty($_POST['name'])) ? $_POST['name'] : '';
    $email_field = $_POST['email'];
    $custom1 = isset($_POST['custom1']) ? $_POST['custom1'] : '';
    $custom2 = isset($_POST['custom2']) ? $_POST['custom2'] : '';
    $body = '';
    if(!empty($name_field)){$body .= "Name: $name_field\n";}
    if(!empty($name_field)){$body .= "E-Mail: $email_field\n";}
    if(!empty($name_field)){$body .= "Custom Input1:: $custom1\n";}
    if(!empty($name_field)){$body .= "Custom Input2:: $custom2\n";}
    $body .= "\r\nThis opt-in came via popup {$campaignID} - {$campname}";
    $headers .="To: {$to}\r\n";
    $headers .="Bcc: {$bcc}\r\n";
    $headers .="Reply-To: $email_field" . "\r\n";
    if(!wp_mail($to, $subject, $body, $headers))
        die('ERROR: wp_mail() failed, please check server mail configuration');
}

if($provider == 'mc')
    include_once('mc/subscribe.php');
elseif($provider == 'cm')
    include_once('campmon/subscribe.php');
elseif($provider == 'aw')
    include_once('aweber_api/subscribe.php');
elseif($provider == 'cc')
    include_once('concon/subscribe.php');
elseif($provider == 'ic')
    include_once('icon/subscribe.php');
elseif($provider == 'gr')
    include_once('getre/subscribe.php');
elseif($provider == 'form' || $provider == 'aw')
    die('formcode');
    
die();
