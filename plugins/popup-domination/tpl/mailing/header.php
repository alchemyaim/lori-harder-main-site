<div id="popup_domination_tabs_main" class="campaign-details">
	<div id="mailing-info">
		<div id="mailing-name-box">
			<label for="config_name">Configuration Name:</label>
			<input id="config_name" name="config_name" type="text" value="<?PHP echo isset($config_name) ? $config_name :'';?>" placeholder="Campaign Name..." />
			<div class="clear"></div>
			<p class="microcopy">e.g. PopUp Domination list</p>
		</div>
		<div id="mailing-description">
			<label for="config_desc">Mailing Configuration Description:</label>
			<input id="config_desc" type="text" name="config_desc" value="<?PHP echo isset($config_desc) ? $config_desc : '';?>" placeholder="Campaign Description..." />
			<p class="microcopy">e.g. List used to track all users who have opted in from PopUp Domination</p>
		</div>
	</div>
	<div class="clear"></div>
</div>
<div style="border-bottom: 1px solid #ccc">
                            <?PHP if(!empty($provider)) { ?>
                                <!-- Shows which mailing list provider the user is connected to -->
                                <div id="connected-provider" style="width: 50%; margin: 20px auto; background: #eee; border-radius: 10px">
                                    <div id="current-connect" style="width: 49%; display: block; float: left; text-align: center">
                                        <p id="currently-connected">You are currently connected to:</p>
                                        <?PHP
                                        if($provider == 'mc')
                                            $logo = '<img src="' . $this->plugin_url . 'css/img/mailchimp.png" alt="mailchimp" />';
                                        else if($provider == 'aw')
                                            $logo = '<img src="' . $this->plugin_url . 'css/img/aweber.png" style="margin-left:-13px;" alt="AWeber"/>';
                                        else if($provider == 'ic')
                                            $logo = '<img src="' . $this->plugin_url . 'css/img/icontact.png" alt="iContact"/>';
                                        else if($provider == 'cc')
                                            $logo = '<img src="' . $this->plugin_url . 'css/img/constant.png" alt="ConstantContact"/>';
                                        else if($provider == 'cm')
                                            $logo = '<img src="' . $this->plugin_url . 'css/img/campaign.png" alt="Campaign Monitor"/>';
                                        else if($provider == 'gr')
                                            $logo = '<img src="' . $this->plugin_url . 'css/img/response.png" alt="Get Response"/>';
                                        else if($provider == 'nm')
                                            $logo = '<img src="' . $this->plugin_url . 'css/img/email.png" alt="Opt In to Email" />';
                                        else if($provider == 'form')
                                            $logo = '<img src="' . $this->plugin_url . 'css/img/htmlform.png" alt="HTML Form Code" />';
                                        else
                                            $logo = '';
                                        ?>
                                        <p id="mailing-provider"><?PHP echo $logo; ?></p>
                                    </div>
                                    <div style="width: 50%; display: block; float: left; text-align: center" id="connected-list" <?PHP echo ($provider == 'nm' || $provider == 'form') ? 'style="display:none;"' : ''; ?>>
                                        <p id="connect-mailing-list">Mailing List you are currently using:</p>
                                        <p id="mailing-list"><?PHP echo $listname; ?></p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            <?PHP } ?>
                           </div>