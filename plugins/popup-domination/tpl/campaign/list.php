<?PHP
/*
* list.php
*
* PHP file used to display all current campaigns
*/
?>
<div class="wrap" id="popup_domination">
    <?PHP
        $header_link = 'Campaign Management';
        $header_url = '#';
        include $this->plugin_path.'tpl/header.php';
    ?>
    <div style="display:none" id="popup_domination_hdn_div"><?PHP echo $fields?></div>
    <div class="clear"></div>
    <div style="padding: 20px; border: 3px solid #ff9a9a; background: #fff0f0; color: #333;"><strong>Update:</strong> We just released PopUp Domination V4, a brand new web app for creating high converting popups. It’s faster than ever, 100% mobile-responsive, even better themes and even better features! <a style="color: #008ace; text-decoration: underline;" target="_blank" href="http://popupdomination.com">Visit PopUpDomination.com today for  instant access!</a></div>
    <div id="popup_domination_container" class="has-left-sidebar">
        <div style="display:none" id="popup_domination_hdn_div2"></div>
        
        <div class="mainbox" id="popup_domination_campaign_list">
            <div class="popdom_contentbox_inside helpbox" style="display: none">
                <p><strong>How to first set up your PopUp Domination</strong></p>
                <iframe width="560" height="315" src="//www.youtube.com/embed/LKNM6enZysU?html5=1" frameborder="0" allowfullscreen></iframe>
                <p>Use the buttons below to create new campaigns</p>
                <ul>
                    <li>Popup campaigns provide a lightbox overlay to the page</li>
                    <li>In-post campaigns appear at the end of your posts content if you choose the pages and/or anywhere you paste the shortcode in a post, page or sidebar widget</li>
                    <!--<li>Sidebar campaigns provide a shortcode to paste into a text widget for your sidebar</li>-->
                </ul>
            </div>
            <div class="popdom_contentbox the_help_box">
                <?PHP if ((isset($header_link) && $header_link != '') &&  (isset($header_url) && $header_url != '')): ?>
                    <h3><a href="<?PHP echo $header_url; ?>"><?PHP echo $header_link; ?></a></h3>
                <?PHP else: ?>
                    <h3><a href="#">&lt; Home</a></h3>
                <?PHP endif; ?>
                <div class="clear"></div>
            </div>
    
            <div class="newcampaign">
                <a class="green-btn" href="<?PHP echo 'admin.php?page='.$this->menu_url.'campaigns&action=create'; ?>"><span>Add New Popup Campaign</span></a>
                <a class="green-btn" href="<?PHP echo 'admin.php?page='.$this->menu_url.'campaigns&action=create&type=inline'; ?>"><span>Add New In-post Campaign</span></a>
                <!--<a class="green-btn" href="<?PHP echo 'admin.php?page='.$this->menu_url.'campaigns&action=create&type=sidebar'; ?>"><span>Add New Sidebar Campaign</span></a>-->
                <p class="campaign-notice">You have <span id="row_count"><?PHP echo $count; ?></span> campaign(s).</p>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            
            <?PHP foreach ($campaigns as $campaign): ?>
            <div class="camprow" id="camprow_<?PHP echo $campaign['id']; ?>" title="<?PHP echo $campaign['id']; ?>">
                <div class="tmppreview">
                    <div class="preview_crop">
                        <div class="spacing">
                            <img class="img" src="<?PHP echo $campaign['previewurl']; ?>" height="<?PHP echo $campaign['height']; ?>" width="<?PHP echo $campaign['width']; ?>" />
                        </div>
                    </div>
                </div>
                <div class="namedesc">
                    <?PHP 
                        $type = ($campaign['inpost']) ? "&type=inline" : "" ;
                    ?>
                    <a href="<?PHP echo 'admin.php?page='.$this->menu_url.'campaigns&action=edit&id='.$campaign['id'].$type; ?>"><?PHP echo $campaign['name']; ?></a><br/>
                    <?PHP
                        $type = "Popup"; 
                        if (!empty($campaign['inpost'])) $type = "In-post";
                        if (!empty($campaign['sidebar'])) $type = "Sidebar";
                        if (!empty($campaign['popup'])) $type = "Popup";
                    ?>
                    <p class="description"><?PHP echo $campaign['desc']; ?></p>
                    <p class="campstyle"><?PHP echo $type ?> Campaign</p>
                    <?PHP if ($type == "In-post") { ?>
                        <p class="popdom-quick-shortcode">Shortcode: <input type="text" value="[popdom id='<?PHP echo $campaign['id']?>']" readonly style="border:none;text-align:center;"/></p>
                    <?PHP } ?>
                </div>
                <div class="analytics" style="display: inline-block; margin-left: 50px;color:black">
                    <!--<div><strong>Month: </strong> <span><?php echo $campaign['month_stats']->conversions; ?> conversions</span> /
                    <span><?php echo $campaign['month_stats']->views; ?> views</span> = <strong><?php echo round(@($campaign['month_stats']->conversions / $campaign['month_stats']->views)*100, 1); ?>% conversion rate</strong></div>-->
                    <div style="font-weight: 300; font-size: 14px; color: #333"><span>Overall: <?php echo $campaign['lifetime_stats']->conversions; ?></span> /
                    <span><?php echo $campaign['lifetime_stats']->views; ?></span> = <span style="font-size: 24px"><?php echo round(@($campaign['lifetime_stats']->conversions / $campaign['lifetime_stats']->views)*100, 1); ?>%</span></div>
                    
                </div>
                <ul class="actions">
                    <li><a title="<?PHP echo $campaign['name']; ?>" class="view_analytics" href="admin.php?page=<?PHP echo $this->menu_url; ?>analytics&id=<?PHP echo $campaign['id'] ?>">Analytics</a></li>
                    <li><a data-id="<?PHP echo $campaign['id']; ?>" title="<?PHP echo $campaign['name']; ?>" class="copy_button" href="#copy">Duplicate</a></li>
                    <li><a data-id="<?PHP echo $campaign['id']; ?>" title="<?PHP echo $campaign['name']; ?>" class="toggle_button <?PHP echo ($campaign['active']) ? 'off':'on';?>" href="#toggle"><?PHP echo ($campaign['active']) ? 'ON | <span style="color:silver">OFF</span>':'<span style="color:silver">ON</span> | OFF';?></a></li>
                    <li><a data-id="<?PHP echo $campaign['id']; ?>" title="<?PHP echo $campaign['name']; ?>" class="deletecamp thedeletebutton" href="#deletecamp">Delete</a></li>
                </ul>
                <div class="clear"></div>
                <?PHP if($campaign['active'] && !in_array($campaign['filename'], $this->no_form_themes) && !$campaign['mailinglist_set']) { ?>
                <div class="popdom_info_warning">
                    <strong>Warning:</strong> A Mailing List is not set for this popup. This popup will <b>not</b> show form inputs until one is set.<br />
                    <strong>Tutorial:</strong> <a href="http://popupdomination.com/help/knowledgebase/setting-up-for-the-first-time/" target="_blank">Setting up for the first time</a>
                </div>
                <?PHP } ?>
            </div>
            <?PHP endforeach; ?>
            
        </div>
        <div class="clearfix"></div>
    <?PHP
        $page_javascript = '';
        $page_javascript = 'var popup_domination_delete_table = "campaigns", popup_domination_delete_stats = "";';
        include $this->plugin_path.'tpl/footer.php'; 
    ?>
    </div>
</div>
