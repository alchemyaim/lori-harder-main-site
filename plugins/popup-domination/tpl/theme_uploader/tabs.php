<div id="popup_domination_tabs" class="tab-menu">
	<a class="icon themebuy selected" href="#themebuy">Theme Installer</a>
	<a class="icon themeupload" href="#themeupload">Theme Upload</a>
	<?PHP if(get_option('popup_domination_theme_updater', false)) { ?><a class="icon themeupdate" href="#themeupdate">Theme Update</a><?PHP } ?>
	<a class="icon themeremove" href="#themeremove">Theme Removal</a>
</div>